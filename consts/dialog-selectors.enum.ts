export enum DialogsSelectors {
    tutor_new = 'tutor-new-dialog',
    tutor_info = 'tutor-information-dialog',
    student_info = 'student-information-dialog',
    courses_list = 'courses-list-dialog',
    course_details = 'course-details-dialog',
    courses_finder = 'courses-finder-dialog',
    courses_payment = 'courses-payment-dialog',
    courses_receipt = 'courses-receipt-dialog'
}
