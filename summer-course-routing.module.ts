
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { TermsConditionsSCComponent } from '../../components/terms-conditions-sc/terms-conditions-sc.component';
import { SummerCourseComponent } from './summer-course.component';

import { SummerCourseGuard } from './summer-course-guard.guard';
import { AuthGuardAdmissionInProcess } from '../userAdministration/auth-guard-userAdministration.service';



const routes: Routes = [
  { 
    path: 'summer-courses/:paymentid',
    component: SummerCourseComponent,
    data: { showAdminMenu: 1 }
  },
  { 
    path: 'summer-courses',
    component: SummerCourseComponent,
    canActivate: [SummerCourseGuard],
    data: { showAdminMenu: 1 }
  },
  { 
    path: 'summer-courses/admin-view/:id_family',
    component: SummerCourseComponent,
    canActivate: [SummerCourseGuard],
    data: { showAdminMenu: 1 }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SummerCourseRoutingModule { }
