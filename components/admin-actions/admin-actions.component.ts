import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from '../../../../../../../core/storage/storage.service';



@Component({
    selector: 'app-admin-actions',
    templateUrl: './admin-actions.component.html',
    styleUrls: ['./admin-actions.component.scss']
})
export class AdminActionsComponent implements OnInit {

    @Input() type: string;

    admin_menu: any[] = [];

    constructor(public storageService: StorageService) {}
    
    ngOnInit(): void {

        let admin_menu: any [] = [
            {
                title: 'ADM TUTOR',
                icon: 'dialpad',
                event: 'actionX',
                role: ['Administrador','SuperAdmin'],
                type: ['tutor']
            },
            {
                title: 'DCT ALL',
                icon: 'dialpad',
                event: 'actionX',
                role: ['Doctor'],
                type: ['student', 'tutor']
            },
            {
                title: 'ADM ALL',
                icon: 'voicemail',
                event: 'actionX',
                role: ['Administrador','SuperAdmin'],
                type: ['student', 'tutor']
            },
            {
                title: 'ADM STUDENT',
                icon: 'notifications_off',
                event: 'actionX',
                role: ['Administrador','SuperAdmin'],
                type: ['student']
            }
        ];

        this.admin_menu = admin_menu
            .filter(menu => menu.role.some(profile => profile === this.storageService.retrieveSession('profile')))
            .filter(menu => menu.type.some(type => type === this.type));

    }

    actionX(): void{
        console.log("entra en event 1");
    }
}