import { Component, ViewEncapsulation, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Family, Student, Tutor, Course, Discount, CourseDiscount, CourseCheckout, CoursesOptions, AdditionalService } from '../../models';

import * as _ from "underscore";
import { CourseReduced } from '../../models/course-discounts.model';
import { AnnouncementCheckout, AnnouncementDiscountCheckout } from '../../models/course-checkout.model';
import { StudentCheckout } from '../../models/checkout.model';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../../../../environments/environment';
import { RefactorCDVService } from '../../services';



@Component({
    selector: 'app-purchase-list',
    templateUrl: './purchase-list.component.html',
    styleUrls: ['./purchase-list.component.scss']
})
export class PurchaseListComponent implements OnInit {
    @Output() checkout: EventEmitter<any> = new EventEmitter();
    @Output() amount: EventEmitter<any> = new EventEmitter();
    @Output() schoolsToPay: EventEmitter<any> = new EventEmitter();
    @Output() international: EventEmitter<boolean> = new EventEmitter();
    @Output() internationalReservation: EventEmitter<boolean> = new EventEmitter();
    @Input() family: Family;
    
    id_students_selected = [];
    tutors: any[];
    discounts = [];
    total_amount = 0;
    final_amount = 0;
    studentCheckout : StudentCheckout[] = [];
    schools: Set<string>;
    isInternational: boolean = false;
    studentsToPay: Partial<Student>[];
    internationalFlag: boolean;
    totalTransport: number;
    get CATALUNYA_PRICE(): number{
		return 14.75 
	} 

    constructor(public translate: TranslateService, private refactorCDVService: RefactorCDVService) {}

    ngOnInit() {
        this.tutors = this.family.tutors;
        this.filterByCoursesUnpaid();
        this.updateAdditionalServices();
        this.selectedAll();
        this.generateDiscounts();
        this.onSelectStudentToPay();
    }

    selectedAll(){
        this.studentsToPay.map( s => {
            this.id_students_selected.push(s.id)
        });
    }

    filterByCoursesUnpaid(){

        this.studentsToPay = JSON.parse(JSON.stringify(this.family.students)).map(s => new Student(s));
        this.studentsToPay = this.studentsToPay.filter(s => { return s.courses.filter( c => { return !c.id_receipt}).length > 0 });
        this.studentsToPay.map( s => { s.courses = s.courses.filter( c => { return !c.id_receipt }) });

    }

    updateAdditionalServices(){
        const one = 1;
        const both = 2;
        this.studentsToPay.forEach(s => {
            let courses : Course[] = s.courses
            let coursesGroupedBySchool = _.groupBy(courses, courses => courses.school.id);
            for (let key in coursesGroupedBySchool){
                const totalTransport = this.getTransportConvos(coursesGroupedBySchool[key]);
                const totalOneTurn = this.getAnotherConvosOfTurns(coursesGroupedBySchool[key], one);
                const totalBothTurn = this.getAnotherConvosOfTurns(coursesGroupedBySchool[key], both);
                coursesGroupedBySchool[key].forEach(c =>  this.setAdditionalServicePrices(c, s, totalTransport, totalOneTurn, totalBothTurn))
                s.courses.forEach(c => c.additional_ser.forEach(as => as.price = coursesGroupedBySchool[c.school.id].find(cour => cour.id === c.id).additional_ser.find(aserv => aserv.id === as.id).price  ))
            } 
        })
    }

    getTransportConvos(courses: Course[]){
        return courses.reduce( (a,b) => {
            let sum = 0
            if(b.additional_ser.find(as => as.id === environment.transportService)){
                sum = a + b.announcements.length
            }else{
                sum = a
            }
            return sum
        }, 0)
    }

    getAnotherConvosOfTurns(courses, quantity){
		return courses.reduce((a, b) => {
			let sum
			 if(b.additional_ser.filter(as => as.id !== environment.transportService).length === quantity){
				 sum = a + b.announcements.length
			 }else{
				 sum = a
			 } return sum }, 0);
    }
    
    async setAdditionalServicePrices(course: Course, student, totalTransport, totalOneTurn, totalBothTurn){
        const course_options = this.setOptions(student);
        course = await this.setOriginalAdditionalServicePrices(course, course_options);
        this.setPrices(course, totalTransport, totalOneTurn, totalBothTurn);
    }

    setPrices(course, totalTransport, totalOneTurn, totalBothTurn) {
		if (this.areBothTurns(course)) {
			course.additional_ser.map(as => {
				if (as.id !== environment.transportService) as.price = this.isCatalunya(course) ? this.CATALUNYA_PRICE : this.roundFiveOrTenth(as.price * 0.84)
			})
			this.setServicePrices(course, totalTransport, totalOneTurn, totalBothTurn);
		} else {
			const turn = course.additional_ser.find(as => as.id !== environment.transportService)
			course.additional_ser.map(as => {
				if (as.id !== environment.transportService) as.price = turn.price
			})
			this.setServicePrices(course, totalTransport, totalOneTurn, totalBothTurn);
		}
    }

    public setServicePrices(course, totalTransport, totalOneTurn, totalBothTurn) {
		let totalConvos = 0;
		course.additional_ser.map(as => {
			if(as.id === environment.transportService){
				totalConvos = totalTransport
			}else if(this.areBothTurns){
				totalConvos = totalBothTurn
			}else{
				totalConvos = totalOneTurn
			}
			as.price = this.setPriceByTerms(as, totalConvos, course) 
		})
    }
    
    public setPriceByTerms(as: Partial<AdditionalService>, totalConvos: number, course) {
		const transport = as.id === environment.transportService
		const percentage = as.id !== environment.transportService ? 0.78 : 0.66
		totalConvos = (totalConvos > 4) ? 4 : totalConvos
		switch (totalConvos) {
			case 2:
				return this.isAlboranOrCatalunya(course) && !transport? Math.round(as.price * 2 * percentage) / 2 : this.roundFiveOrTenth(as.price * 2 * percentage) / 2
			case 3:
				let price = this.isAlboranOrCatalunya(course) && !transport? Math.round(as.price * 2.68 * percentage) / 3 : this.roundFiveOrTenth(as.price * 2.68 * percentage) / 3
				if(this.areBothTurns(course) && !this.isAlboranOrCatalunya(course)) price += 0.6666666666666667
				return price
			case 4:
				return this.isAlboranOrCatalunya(course) && !transport? Math.round(as.price * 3.33 * percentage) / 4 : this.roundFiveOrTenth(as.price * 3.33 * percentage) / 4
			default:
				return as.price
		}
    }
    
    isAlboranOrCatalunya(course): boolean{
        const id = course.school.id_mysek;
		return id === environment.catalunya || id === environment.alboran
    }

    roundFiveOrTenth(amount) {
		const multiple10 = Math.round(amount / 10) * 10;
		const multiple5 = Math.round(amount / 5) * 5;
		const diff10 = Math.abs(amount - multiple10);
		const diff5 = Math.abs(amount - multiple5);
		diff10 < diff5 ? amount = multiple10 : amount = multiple5;
		return amount
	}
    
    isCatalunya(course): boolean{
        const id = course.school.id_mysek;
        return id === environment.catalunya
	}

    async setOriginalAdditionalServicePrices(course, course_options){
        const original_course = new Course(JSON.parse(JSON.stringify(await this.refactorCDVService.getCourse(course.id, course_options))));
        const original_prices = this.getOriginalPricesMap(original_course);
        course.additional_ser.forEach( as => as.price = original_prices.get(as.id));
        return course;
    }

    getOriginalPricesMap(original_course){
        let original_prices = new Map<string, number>();
        original_course.additional_ser.forEach(as => original_prices.set(as.id, as.price))
        return original_prices
    }

    setOptions(student){

		const options = {
			id_student : student.id,
			check_paid : true,
			use_alexia_prices: true
		}
		return new CoursesOptions(options);
		
    }
    
    areBothTurns(course) {
		const result = _.filter(course.additional_ser, (s: any) => {
			return (s.id === "01t26000005ZrHaAAK" || s.id === "01t26000005ZrHbAAK" || s.id === "01t1n00000EAw1nAAD" || s.id === "01t1n00000EAw1iAAD") && s.selected
		})
		return result.length === 2
	}

    generateDiscounts(){

        let students: Partial<Student>[] = this.studentsToPay;

        
        for (let stud_idx = 0; stud_idx < students.length; stud_idx++) {
            this.discounts[stud_idx] = []

            for (let cours_idx = 0; cours_idx < students[stud_idx].courses.length; cours_idx++) {
                let discount = this.applyCourseDiscount(students[stud_idx].courses[cours_idx]);
                this.discounts[stud_idx][cours_idx] = discount || undefined;
            }
        }

        
    }

    applyCourseDiscount(course: Course): any {

        const course_discount = new CourseDiscount(course, this.tutors, this.translate);
        return course_discount
    }

    onSelectStudentToPay( id?: Number, isChecked?: Boolean){
        if(id){
            if (isChecked) {
                this.id_students_selected.push(id);
            } else {
                let index = this.id_students_selected.findIndex(x => x == id);
                this.id_students_selected.splice(index, 1);
                this.isInternational = false;
            }
        }
        this.isInternationalReserve();
        this.getTotalAmount();
        this.getFinalStatus();
        this.checkout.emit(this.studentCheckout);
        this.amount.emit(this.final_amount);
        this.schoolsToPay.emit(this.schools);
        this.international.emit(this.isInternational);
        this.internationalReservation.emit(this.internationalFlag);
    }

    isInternationalReserve(){
        
        const students = this.studentsToPay.filter( s => _.contains(this.id_students_selected, s.id));
        const interFlag = students.find( s => (s.courses.find( c => c.inter_fields !== null ) ? true : false )) ? true : false;
        if(interFlag) this.internationalFlag = students.find( s => (s.courses.filter( c => c.inter_fields ? c.inter_fields.reservation_international : false ) ? true : false )) ? true : false;

    }

    getFinalStatus() {
        
        let students: Partial<Student>[] = this.studentsToPay;
        this.final_amount = 0;
        this.studentCheckout = [];
        this.schools = new Set();
        for (let stud_idx = 0; stud_idx < students.length; stud_idx++) {
            let coursesStudent : CourseCheckout[] = [];
            if (_.contains(this.id_students_selected, students[stud_idx].id)){
                for (let cours_idx = 0; cours_idx < students[stud_idx].courses.length; cours_idx++) {
                    let courseCheckout : CourseCheckout
                    this.schools.add(students[stud_idx].courses[cours_idx].school.id_mysek)
                    if(!this.isInternational){
                        this.isInternational = students[stud_idx].courses[cours_idx].school.international
                    }
                    if (this.discounts[stud_idx][cours_idx]) {
                        this.final_amount += this.discounts[stud_idx][cours_idx].course_reduced.final_amount;
                        this.final_amount += this.discounts[stud_idx][cours_idx].course_reduced.additional_services.reduce((s, f) => {
                            return (f.final_amount || f.final_amount === 0) ? s + f.final_amount : s + f.initial_amount;              
                    }, 0);
                        courseCheckout = this.setCourseCheckout(students[stud_idx].courses[cours_idx], this.discounts[stud_idx][cours_idx].course_reduced);
                    } else {
                        let amount = (students[stud_idx].courses[cours_idx].announcements[0].price) * students[stud_idx].courses[cours_idx].announcements.length;
                        amount += students[stud_idx].courses[cours_idx].additional_ser.reduce((s, f) => {
                                return s + f.price * students[stud_idx].courses[cours_idx].announcements.length;              
                        }, 0);
                        this.final_amount += amount;
                        courseCheckout = this.setCourseCheckout(students[stud_idx].courses[cours_idx]);
                    }
                    coursesStudent.push(courseCheckout);
                }
                let item = {
                    courseLines : coursesStudent,
                    id: students[stud_idx].id
                }
                this.studentCheckout.push(new StudentCheckout(item));
            }
        }
    }

    getTotalAmount() {

        let students: Partial<Student>[] = this.studentsToPay
        this.total_amount = 0;


        for (let stud_idx = 0; stud_idx < students.length; stud_idx++) {
            for (let cours_idx = 0; cours_idx < students[stud_idx].courses.length; cours_idx++) {

                if (_.contains(this.id_students_selected, students[stud_idx].id)) {

                    this.total_amount += (students[stud_idx].courses[cours_idx].announcements[0].price) * students[stud_idx].courses[cours_idx].announcements.length;
                    this.total_amount += students[stud_idx].courses[cours_idx].additional_ser.reduce((s, f) => {
                        return s + f.price * students[stud_idx].courses[cours_idx].announcements.length;              
                }, 0);
                }
            }
        }
    }

    setAnnouncementCheckout(student_course: Course): AnnouncementCheckout[] {
        let announcements : AnnouncementCheckout[] = [];
        for(let i = 0; i < student_course.announcements.length; i++){
            let item = {
                id: student_course.announcements[i].id,
                initial_amount: student_course.announcements[i].price,
            }
            let announcementCheckout = new AnnouncementCheckout(item);
            announcements.push(announcementCheckout);
        }
        return announcements
    }

    setAnnouncementDiscounts(final_course : CourseReduced): AnnouncementDiscountCheckout[]{
        let announcements_discounts : AnnouncementDiscountCheckout[] = [];
        for(let i = 0; i < final_course.discounts.length; i++){
            let item = {
                name: final_course.discounts[i].name,
                amount: final_course.discounts[i].amount,
                percentage: final_course.discounts[i].percentage
            }
            let discount = new AnnouncementDiscountCheckout(item);
            announcements_discounts.push(discount)
        }
        return announcements_discounts;
    }

    setCourseCheckout(student_course: Course, final_course?: CourseReduced) : CourseCheckout{
        const announcements = this.setAnnouncementCheckout(student_course);
        let item
        if(final_course){
            const announcements_discounts = this.setAnnouncementDiscounts(final_course)
            item = {
                id_opportunity: student_course.announcements[0].id_opportunity,
                announcements,
                announcements_discounts,
                additional_services: final_course.additional_services,
                final_announc_amount: final_course.final_amount
            }
        }else{
            item = {
                id_opportunity:student_course.announcements[0].id_opportunity,
                announcements
            }
        }

        let courseCheckout = new CourseCheckout(item);
        return courseCheckout;
    }

}