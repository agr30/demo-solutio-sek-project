import { Component, ViewEncapsulation, Input, Output, OnInit } from '@angular/core';
import { Announcements } from '../../models';
// import { Student } from '../../../../models';


@Component({
    selector: 'app-announcements',
    templateUrl: './announcement.component.html',
    styleUrls: ['./announcement.component.scss']
	// encapsulation: ViewEncapsulation.None
})
export class AnnouncementComponent implements OnInit {
    @Input() announcement: Announcements;


    constructor() {}

    ngOnInit(): void {}
}