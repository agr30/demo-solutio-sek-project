import { Person } from './person.model';
import { Address } from './address.model';
import { SelectedCourseList } from './selected-course-list.model';
import { Course } from './course.model';


export class Student extends Person {
    public medical_information: string = '';
    // public address: Address;
    public courses: Course[] = [];

    constructor(item?: Partial<Student>) {
        super(item);

        if (!item)return;
        Object.assign(this, item);

        // this.address = new Address(item.address);
        this.courses = (item.courses || []).map(t => new Course(t));

    }
    
}
