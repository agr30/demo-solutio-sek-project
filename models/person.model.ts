export class Person {
    public id: string = '';
    public first_name: string = '';
    public first_surname: string = '';
    public second_surname: string = '';
    public progress: number = 0
    public birth_date?: string = '';
    public gender: string = '';
    public document_type: string = '';
    public document_number: string = '';
    public nationality: string = '';
    get full_name(): string {
        if(this.second_surname){
            return `${this.first_name} ${this.first_surname} ${this.second_surname}`;
        }else{
            return `${this.first_name} ${this.first_surname}`;
        }
        
    }
    
    constructor(item?: Partial<Person>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
    
}
