import { Announcements } from "./announcements.model";

export class AnnouncementsDetail extends Announcements {
    public price: number ;
    
    constructor(item?: Partial<AnnouncementsDetail>) {

        super(item);

        if (!item)return;
        this.price = Number(item.price);
        // Object.assign(this, item);
    }
}
