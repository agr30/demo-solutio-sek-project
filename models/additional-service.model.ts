import { Discount } from "./discount.model";

export class AdditionalService {

    public id: string;
    public id_opportunity: string;
    public name: string;
    public price: number;
    public paid?: boolean;
    public selected?: boolean = false
    public discounts?: any[];
    public initial_amount?: number;
    public final_amount?: number;

    constructor(item?: Partial<AdditionalService>) {
        if (!item) { return; }

        Object.assign(this, item);
        this.price = Number(item.price);
    }
}