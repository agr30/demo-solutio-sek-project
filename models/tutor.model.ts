import { Person } from './person.model';
import { Contact } from './contact.model';
import { Address } from './address.model';
import { Collective } from './collective.model';

export class Tutor extends Person {

    public language: string  = 'es';
    public marital_status: string = '';
    public alumni_SEK: boolean = false;
    public alumni_SEK_name: string = '';
    public sport_club: boolean = false;
    public collective: Collective;
    public accepted_terms: boolean = false;
    public sharing_info: boolean = false;
    public contact: Contact;
    // public address: Address;
    public logged: boolean = false;

    constructor(item?: Partial<Tutor>) {
        super(item);

        if (!item)return;
        Object.assign(this, item);

        this.contact = new Contact(item.contact);
        // this.address = new Address(item.address);
        this.collective = new Collective(item.collective);

    }
}
