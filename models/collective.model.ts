export class Collective {

    public id: string;
    public name: string;
    public discount: number | null;
    public transport: number | null;

    constructor(item?: Partial<Collective>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
}
