export class InterFields {

    public flight_option: string;
    public flight_origin: string;
    public language_level: string;
    public abroad_program: string;
    public program_type_place_duration: string;
    public passport: string;
    public reservation_international: boolean | null;



    constructor(item?: Partial<InterFields>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
}