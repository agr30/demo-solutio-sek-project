import { CourseCheckout } from "./course-checkout.model";

export class StudentCheckout {
    public courseLines: CourseCheckout[];
    public id: string;

    constructor(item?: Partial<StudentCheckout>) {
        if (!item) { return; }
        Object.assign(this, item);
        this.courseLines = (item.courseLines || []).map(c => new CourseCheckout(c));
    }

}


export class Checkout {
    public final_price: number = 0;
    public method: string = ""
    public guid_transaction?: string;
    public date?: string;
    public students: StudentCheckout[];
    public iban?: string;
    public holder?: string;
    public id_mysek: string;

    constructor(item?: Partial<Checkout>) {
        if (!item) { return; }
        Object.assign(this, item);
        this.students = (item.students || []).map(s => new StudentCheckout(s));
    }
}

