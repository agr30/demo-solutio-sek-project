export class Thematic {
    public id: string = '';
    public name: string = '';
    
    constructor(item?: Partial<Thematic>) {
        
        if (!item) { return; }
        Object.assign(this, item);
    }
    
}
