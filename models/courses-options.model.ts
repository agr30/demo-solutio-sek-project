export class CoursesOptions {

    public id_student: string;
    public check_paid: boolean = false;
    public only_chosen: boolean = false;
    public only_not_paid: boolean = false;
    public call_alexia: boolean = false;

    constructor(item?: Partial<CoursesOptions>) {
        if (!item) { return; }
        Object.assign(this, item);
    }

}