import { School } from "./school.model";
import { Thematic } from "./thematic.model";
import { AnnouncementsDetail } from "./announcements-detail.model";
import { AdditionalService } from "./additional-service.model";
import { InterFields } from "./inter_fields.model";

export class Course {
    public id: string = '';
    public id_opportunity: string = '';
    public id_receipt: string | null = null;
    public name: string = '';
    public photo: string = '';
    public date_discount: string = '';
    public initial_birthdate: string = '';
    public final_birthdate: string = '';
    public link: string = '';
    public age_min: number = 0;
    public age_max: number = 100;
    public init: string;
    public end: string;
    public school: School;
    public thematic: Partial<Thematic>[] = [];
    public alumno_sek: boolean | null = false;
    public announcements: Partial<AnnouncementsDetail>[] = [];
    public additional_ser: Partial<AdditionalService>[] = [];
    public inter_fields: Partial<InterFields> | null = null;
    public activities: string | null;
    public full_course: boolean;
    public route?: string;
    


    constructor(item?: Partial<Course>) {

        if (!item)return;
        Object.assign(this, item);

        this.school = new School(item.school);
        this.thematic = (item.thematic || []).map(t => new Thematic(t));
        this.announcements = (item.announcements || []).map(a => new AnnouncementsDetail(a));
        this.additional_ser = (item.additional_ser || []).map(as => new AdditionalService(as));
    }

}
