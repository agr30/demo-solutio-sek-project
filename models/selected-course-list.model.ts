import { Announcements } from './announcements.model';

export class SelectedCourseList {

    public id: string = '';
    public name: string = '';
    public school: string = '';
    public photo: string = '';
    public announcements: Announcements[] = [];
    
    constructor(item?: Partial<SelectedCourseList>) {
        if (!item)return;
        Object.assign(this, item);

        this.announcements = (item.announcements || []).map(c => new Announcements(c));
    }
}
