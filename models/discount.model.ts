export class Discount {
    name: string = '';
    amount: number = 0;
    percentage: number = 0;

    constructor(item?: Partial<Discount>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
}