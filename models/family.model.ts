import { Tutor } from './tutor.model';
import { Student } from './student.model';

export class Family {
 
    public id: string;
    public name: string;
    public monoparental: boolean;
    public marital_status_fam: string;
    public tutors: Partial<Tutor>[] = [];
    public students: Partial<Student>[] = [];

    constructor(item?: Partial<Family>) {

        if (!item) return;

        Object.assign(this, item);
        this.tutors = (item.tutors || []).map(t =>  new Tutor(t));
        this.students = (item.students || []).map(s => new Student(s));
       
    }
}
