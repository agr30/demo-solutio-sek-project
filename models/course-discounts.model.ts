import { Tutor } from './tutor.model';
import { Course } from './course.model';
import * as _ from 'underscore';
import { AdditionalService } from './additional-service.model';
import { Discount } from './discount.model';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../../../environments/environment';



export class CourseReduced {

    public announcement_price: number = 0;
    public initial_amount: number = 0;
    public final_amount: number = 0;
    public discounts: Discount[];
    public additional_services: AdditionalService [];

    constructor(item?: Partial<CourseReduced>) {
        if (!item) { return; }

        this.discounts = (item.discounts || []).map(d =>  new Discount(d));
        this.additional_services = (item.additional_services || []).map(as =>  new AdditionalService(as));
        Object.assign(this, item);
    }
}

export class CourseDiscount {
 
    private course: Course;
    private tutors: Tutor[];
    private quantity: number;
    public course_reduced: CourseReduced;
    private isAlboranOrCatalunya: boolean = false;
    public plusFlag : boolean = false;

    constructor(course : Course, tutors: Tutor[], public translate: TranslateService ) {
        this.course = course;
        this.tutors = tutors;
        this.getTerms();
        this.GetIsAlboranOrCatalunya();
        this.setReducedCourse();
        this.applyCourse();
        this.applyAdditionalServices();
        this.roundFinalAmount();
    }

    setReducedCourse(){
        let additional_services = _.filter( this.course.additional_ser, (a : any) =>{
			return a.selected;
        })
        additional_services.map(as => {
            as.discounts = [];
        })
        const initial_amount = _.filter( this.course.announcements, (a : any) =>{
			return a.selected;}).reduce((s, f) => {
                 return s + f.price;}
                 , 0);
        const announcement_price = initial_amount / this.quantity
        let item = {
           announcement_price,
           initial_amount,
           additional_services
        } 
        this.course_reduced = new CourseReduced(item);
    }

    getTerms(){
        const announcements_selected = _.filter( this.course.announcements, (a : any) =>{
			return a.selected;
        })
        if(announcements_selected) this.quantity = announcements_selected.length;
    }

    applyCourse(){
        
        (this.isSpanishCultureCourse() && this.quantity === 2) ? this.spanishCultureReset() : this.convoDiscount(this.course_reduced, this.course_reduced.announcement_price, 0.84);
        this.addAmountSecondWeek();
        this.earlyPaymentDiscount();
        this.collectiveDiscount();
    }

    applyAdditionalServices(){
        
        for(let i = 0; i < this.course_reduced.additional_services.length; i++){
            let item = this.course_reduced.additional_services[i]
            if(!item.discounts) item.discounts = [];
            item.initial_amount = item.price * this.quantity
            if(item.id === "01t26000006FzyuAAC" || item.id === "01t1n00000EAw1YAAT"){
                item.final_amount = item.price * this.quantity;
                this.applyDiscountCollectiveTransport(item);
            }
        }
    }

    convoDiscount(item: any, price: number, percentage: number){
        let convo_dto;
        let name = "";
        if(this.quantity > 1) this.translate.get('summerCourse.courses.discountWeeks', {quantity : this.quantity}).subscribe(res =>{ name = res;})
        switch(this.quantity){
            case 2:
                item.final_amount = price * 2 * percentage;
                convo_dto = {
                    name,
                    amount : item.id ? this.roundAmount(item.initial_amount - item.final_amount, item) : item.initial_amount - item.final_amount,
                    percentage
                };
                item.discounts.push(convo_dto);
                break;
            case 3:
                item.final_amount = price * 2.68 * percentage;
                convo_dto = {
                    name,
                    amount : item.id ? this.roundAmount(item.initial_amount - item.final_amount, item) : item.initial_amount - item.final_amount,
                    percentage
                };
                item.discounts.push(convo_dto);
                break;
            case 4:
                item.final_amount = price * 3.33 * percentage;
                convo_dto = {
                    name,
                    amount : item.id ? this.roundAmount(item.initial_amount - item.final_amount, item) : item.initial_amount - item.final_amount,
                    percentage
                };
                item.discounts.push(convo_dto);
                break;
            default:
                item.final_amount = price * this.quantity;
        }
    }

    earlyPaymentDiscount(){
        const before = this.course_reduced.final_amount;
        const today = new Date();
        const early_date = new Date(this.course.date_discount);
        const diff = early_date.getTime() - today.getTime();
        let name = "";
        this.translate.get('summerCourse.courses.earlyPayment', {early_date : early_date.toLocaleDateString()}).subscribe(res =>{
            name = res;
        })
        if(diff >= 0){
            this.course_reduced.final_amount = this.course_reduced.final_amount * 0.95;
            const dtoEarly = {
                name,
                amount : before - this.course_reduced.final_amount,
                percentage: 0.05
            };
            this.course_reduced.discounts.push(dtoEarly);  
        } 
    }

    collectiveDiscount(){
        let before = this.course_reduced.final_amount;
        const bestCollectiveTutors = this.getBestCollective();
        const communitySEK = this.getCommunitySEK();
        let name = "";
        if(bestCollectiveTutors.discount > 7.5){
            this.course_reduced.final_amount = this.course_reduced.final_amount * (1 - (bestCollectiveTutors.discount/100));
            this.translate.get('summerCourse.courses.collectiveDiscount', {percentage: bestCollectiveTutors.discount, collective: bestCollectiveTutors.name}).subscribe(res =>{
                name = res;
            })
            const dto_collective = {
                name,
                amount : before - this.course_reduced.final_amount,
                percentage: bestCollectiveTutors.discount/100
            };
            this.course_reduced.discounts.push(dto_collective);
            if(bestCollectiveTutors.name === "PER-IESEK-CDV" || bestCollectiveTutors.name === "DIR-IESEK-CDV"){
                if(this.course.alumno_sek){
                    before = this.course_reduced.final_amount;
                    this.course_reduced.final_amount = this.course_reduced.final_amount * 0.925;
                    this.translate.get('summerCourse.courses.sekStudent').subscribe(res =>{
                        name = res;
                    })
                    let amount = before - this.course_reduced.final_amount
                    const dto_collective = {
                        name,
                        amount,
                        percentage: 0.075
                    };
                    this.course_reduced.discounts.push(dto_collective);
                }
            }
        }else if(this.course.alumno_sek){
            this.course_reduced.final_amount = this.course_reduced.final_amount * 0.925;
            this.translate.get('summerCourse.courses.sekStudent').subscribe(res =>{
                name = res;
            })
            const dto_collective = {
                name,
                amount : before - this.course_reduced.final_amount,
                percentage: 0.075
            };
            this.course_reduced.discounts.push(dto_collective);
        }else if(communitySEK === 'Alumni'){
            this.course_reduced.final_amount = this.course_reduced.final_amount * 0.925;
            this.translate.get('summerCourse.courses.alumniSEK').subscribe(res =>{
                name = res;
            })
            const dto_collective = {
                name,
                amount : before - this.course_reduced.final_amount,
                percentage: 0.075
            };
            this.course_reduced.discounts.push(dto_collective);
        }else if(communitySEK === 'Sport Club' && this.course.thematic[0].name === 'Sports club'){
            this.course_reduced.final_amount = this.course_reduced.final_amount * 0.925;
            this.translate.get('summerCourse.courses.sportClub').subscribe(res =>{
                name = res;
            })
            const dto_collective = {
                name,
                amount : before - this.course_reduced.final_amount,
                percentage: 0.075
            };
            this.course_reduced.discounts.push(dto_collective);
        }else if(bestCollectiveTutors && typeof bestCollectiveTutors.discount === 'number' && bestCollectiveTutors !== -Infinity){
            this.course_reduced.final_amount = this.course_reduced.final_amount * (1 - (bestCollectiveTutors.discount/100));
            this.translate.get('summerCourse.courses.collectiveDiscount', {percentage: bestCollectiveTutors.discount, collective: bestCollectiveTutors.name}).subscribe(res =>{
                name = res;
            })
            const dto_collective = {
                name,
                amount : before - this.course_reduced.final_amount,
                percentage: bestCollectiveTutors.discount/100
            };
            this.course_reduced.discounts.push(dto_collective);
        }
    }

    applyDiscountCollectiveTransport(item){
        const before = item.final_amount;
        const bestCollectiveTransport = this.getBestCollectiveForTransport();
        let name = "";
        if(bestCollectiveTransport){
            item.final_amount = Math.round(item.final_amount * (1 - (bestCollectiveTransport.transport/100)));
            this.translate.get('summerCourse.courses.collectiveDiscount', {percentage: bestCollectiveTransport.transport, collective: bestCollectiveTransport.name}).subscribe(res =>{
                name = res;
            })
            const dto_collective = {
                name,
                amount : before - item.final_amount,
                percentage: bestCollectiveTransport.transport/100
            };
            item.discounts.push(dto_collective);
        }
    }

    getBestCollective(){
        let bestCollectiveTutors = undefined;
        const collectiveTutors = _.chain(this.tutors).pluck('collective').flatten().value();
        if(collectiveTutors.length > 1 ){
            bestCollectiveTutors = _.max(collectiveTutors, (c) => { return c.discount})
        }else if(collectiveTutors[0] !== null){
            bestCollectiveTutors = collectiveTutors[0];
        }
        return bestCollectiveTutors
    }

    getBestCollectiveForTransport(){
        let bestCollectiveTransport = undefined;
        const collectiveTutors = _.chain(this.tutors).pluck('collective').flatten().value().filter(c => { return c.transport > 0 });
        if(collectiveTutors.length > 1 ){
            bestCollectiveTransport = _.max(collectiveTutors, (c) => { return c.transport})
        }else if(collectiveTutors[0] !== null){
            bestCollectiveTransport = collectiveTutors[0];
        }
        return bestCollectiveTransport
    }

    getCommunitySEK(){
        let communitySEK = "";
        if(this.tutors.find( t => t.alumni_SEK)){
            communitySEK = "Alumni";
        }else if(this.tutors.find( t => t.sport_club)){
            communitySEK = "Sport Club";
        } 
        return communitySEK;
    }

    roundFinalAmount(){
        this.course_reduced.final_amount = Math.round(this.course_reduced.final_amount);
    }

    roundAmount(amount, item): number{
        let flag = false
        if(item.id) flag = this.isMorningOrAfternoon(item.id);
        if(this.isAlboranOrCatalunya && flag){
            return Math.round(amount);
        }else{
            return this.roundFiveOrTenth(amount);
        }
    }

    roundFiveOrTenth(amount){
        const multiple10 = Math.round(amount/10)*10;
        const multiple5 = Math.round(amount/5)*5;
        const diff10 = Math.abs(amount - multiple10);
        const diff5 = Math.abs(amount - multiple5);
        diff10 < diff5 ? amount = multiple10 : amount = multiple5;
        return amount
    }

    isMorningOrAfternoon(id){
        return id === "01t26000005ZrHaAAK" || id === "01t26000005ZrHbAAK" || id === "1" || id === "01t1n00000EAw1iAAD" || id === "01t1n00000EAw1nAAD"
    }

    GetIsAlboranOrCatalunya(){
        const id = this.course.school.id_mysek;
        if(id === "2383" || id === "684" || id === "683" || id === "2382"){
            this.isAlboranOrCatalunya = true;
        }
    }

    isCatalunya(): boolean{
        const id = this.course.school.id_mysek;
        return id === "683" || id === "2382"
    }

    isExceptionalCatCourse(){
        const id = this.course.id;
        return id === "0012600000rHHc0AAG" || id === "0011n000021J5ZFAA0" || id === "0012600000rHHc2AAG" || id === "0011n000021J5ZHAA0"
    }

    isSecondWeek(){
        const second = this.course.announcements.find( a => a.id === "a0C2600000FHsr2EAD" || a.id === "a0C2600000FHsquEAD" || a.id === "a0C1n00001P1hdfEAB" || a.id === "a0C1n00001P1hdnEAB" )
        if(second){
            return second.selected
        }else{
            return false
        }
    }

    addAmountSecondWeek(){
        if(this.isExceptionalCatCourse()){
            if(this.isSecondWeek()){
                this.course_reduced.final_amount+= 120;
                this.plusFlag = true
            }     
        }
    }

    spanishCultureReset(){
        if(this.course.id === environment.spanishIntensiveWithResidency){
            this.course_reduced.final_amount = 2203
        }

        if(this.course.id === environment.spanishIntensiveWitoutResidency){
            this.course_reduced.final_amount = 943
        }
    }

    isSpanishCultureCourse(): boolean{
        return this.course.id === environment.spanishIntensiveWithResidency || this.course.id === environment.spanishIntensiveWitoutResidency
    }

}