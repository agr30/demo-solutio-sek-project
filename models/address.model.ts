export class Address {

    public residence: string;
    public town: string;
    public zip: string;
    public province: string;
    public country: string;

    constructor(item?: Partial<Address>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
}
