export class Announcements {
    public id: string = '';
    public id_opportunity: string;
    public init: string;
    public end: string;
    public paid?: boolean;
    public selected?: boolean = false;
    public status?: string;

    constructor(item?: Partial<Announcements>) {

        if (!item)return;
        Object.assign(this, item);
    }
}
