export class Contact {

    public email: string;
    public phone: string;

    constructor(item?: Partial<Contact>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
}
