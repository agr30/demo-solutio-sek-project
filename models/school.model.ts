export class School {
    public id: string = '';
    public id_mysek: string = '';
    public name: string = '';
    public international: boolean = false;
    
    constructor(item?: Partial<School>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
    
}
