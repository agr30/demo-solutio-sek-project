import { AdditionalService } from "./additional-service.model";

export class AnnouncementCheckout {
    public id: string;
    public initial_amount: number;

    constructor(item?: Partial<AnnouncementCheckout>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
}

export class AnnouncementDiscountCheckout {
    public percentage: number = 0;
    public name: string;
    public amount: number;

    constructor(item?: Partial<AnnouncementDiscountCheckout>) {
        if (!item) { return; }
        Object.assign(this, item);
    }
}

export class CourseCheckout {
    public id_opportunity: string;
    public final_announc_amount: number;
    public additional_services: AdditionalService[];
    public announcements: AnnouncementCheckout[];
    public announcements_discounts: AnnouncementDiscountCheckout[];


    constructor(item?: Partial<CourseCheckout>) {
        if (!item) { return; }
        Object.assign(this, item);
        this.additional_services = (item.additional_services || []).map(as => new AdditionalService(as));
        this.announcements = (item.announcements || []).map(a => new AnnouncementCheckout(a));
        this.announcements_discounts = (item.announcements_discounts || []).map(ad => new AnnouncementDiscountCheckout(ad));
    }
}