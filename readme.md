# Demo SEK Front-End Project

This repo is only intended to be a demo about my code experience in other projects.
It doesn't contains all the code, and is not deployable or installable for development.

Please, if you want a demo of, and I'm able to do it, contact me by e-mail.

## Explaination of demo

This code is part of a complete PWA Angular Project with Ionic deployed on Azure.

Main technology used is **Angular 7** with Material Angular, Bootstrap, SASS and many other libraries like moment.js.
It main purpose is to be a control panel for fathers that their sons are studying at SEK College.

Enjoy the code!