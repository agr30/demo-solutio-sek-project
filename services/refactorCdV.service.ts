import { Injectable } from '@angular/core';///
// import { Student, StudentList, StudentSearchList } from '../models';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
// import { NotificationService } from './notification.service';
import { Observable } from 'rxjs/Observable';
import { Family, Tutor, Course, Checkout, CoursesOptions } from '../models';
import { StorageService } from '../../../../../../core/storage/storage.service';
import { ConfigResourceService } from '../../../../../../core/config/config.resource.local';

@Injectable()
export class RefactorCDVService {

  servicesRefactorSM: string;
  servicesUrl: string;

  constructor(
    private http: HttpClient,
    // private notifier: NotificationService,
    public storageService: StorageService,
    public config: ConfigResourceService,
  ) 
  {
    this.servicesRefactorSM = this.config.getServicesRefactorSM_URL();
    this.servicesUrl = this.config.getServicesUrl();
  }

  ////********* FAMILY */

  async getFamily(id: string): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Families/${id}`
    return this.http.get(url).toPromise();
  }

  async checkRequiredFields(id_family){
    const url: string = `${this.servicesRefactorSM}/Families/CheckRequiredFields/cdv/${id_family}`
    return this.http.get(url, { observe: 'response' }).toPromise();
  }

  async getBackoffice(family_form_falue,page_size, last_member ): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/BackOffice`
    let httpParams = new HttpParams(); 
    Object.keys(family_form_falue).forEach(function (key) {  
        if (family_form_falue[key] != ""){
          if(key == "admission_status" || key == "courses" || key == "schools"){
            family_form_falue[key].forEach(function (value) {
              httpParams = httpParams.append(key, value); 
            });  
          }else{
            httpParams = httpParams.append(key, family_form_falue[key]);
          }  
        }
    }); 
    httpParams = httpParams.append("LastMember", last_member);
    httpParams = httpParams.append("PageSize", page_size);
    return this.http.get(url, { params: httpParams, observe: 'response' }).toPromise().then( resp => {
      return resp;
    }); 
    
  }
  
  ////********* TUTOR */
  async updateTutor(id: string, tutor_form_falue): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Tutors/${id}`
    return this.http.put(url, tutor_form_falue,  { observe: 'response' }).toPromise();
  }
  
  async saveTutor(tutor_form_falue): Promise<any>  {
    const url: string = `${this.servicesRefactorSM}/Tutors/${this.storageService.retrieveSession('idFamilySelected')}`
    return this.http.post(url, tutor_form_falue,  { observe: 'response' }).toPromise();
  }
  
  async getTutor(id: string): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Tutors/${id}`
    return this.http.get(url, { observe: 'response' }).toPromise();
  }

  async createTutor(id: string, tutor_form): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Tutors/${id}`
    return this.http.post(url, tutor_form, { observe: 'response' }).toPromise();
  }

  ////********* STUDENT */
  
  async updateStudent(id, student_form_falue): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Students/${id}`
    return this.http.put(url, student_form_falue,  { observe: 'response' }).toPromise();
    
  }
  
  async saveStudent(student_form_falue): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Students/${this.storageService.retrieveSession('idFamilySelected')}`
    return this.http.post(url, student_form_falue,  { observe: 'response' }).toPromise();
  }
  
  async getStudent(id: string): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Students/${id}`
    return this.http.get(url, { observe: 'response' }).toPromise();
  }
  
  ////********* COURSES */
  async getCourse(id_course: string, course_options: CoursesOptions): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Courses/${id_course}`
    let httpParams = new HttpParams();
    Object.keys(course_options).forEach(function (key) {
          httpParams = httpParams.append(key, course_options[key]);
      });
    
    return this.http.get(url, { params: httpParams, observe: 'response' }).toPromise().then( resp => {
      return resp.body;
    });
  }

  async getCourses(): Promise<any>  {
    const url: string = `${this.servicesRefactorSM}/Courses`
    return this.http.get(url, { observe: 'response' }).toPromise();
    // mock
    //return this.http.get<Course[]>('app/assets/mocks/courses.json').toPromise();
  }

  async saveCourse(id: string, course: Course): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Courses?studentId=${id}`
    return this.http.post(url, course, { observe: 'response' }).toPromise();
  }

  async editCourse(id: string, course: Course): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Courses?studentId=${id}`
    return this.http.put(url, course, { observe: 'response' }).toPromise();
  }

  async deleteCourse(id_opportunity: string): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Courses/${id_opportunity}`
    return this.http.delete(url, { observe: 'response' }).toPromise();
  }

   ////********* PAYMENT */

   async getPayment(id_payment: string): Promise<any> {
    const url: string = `${this.servicesRefactorSM}/Payment/${id_payment}`
    return this.http.get(url, { observe: 'response' }).toPromise();
  }

  async makePayment(body: Checkout){
    const url: string = `${this.servicesRefactorSM}/Payment`
    return this.http.post(url, body, { observe: 'response' }).toPromise();
  }
  
  async updateStatusPayment(id_payment, status){
    const url: string = `${this.servicesRefactorSM}/Payment/${id_payment}?status=${status}`
    return this.http.put(url, { observe: 'response' }).toPromise();
  }

 
  
  ////********* SCHOOLS */
  async getSchools(): Promise<any> {
    const url: string = `${this.servicesUrl}/api/schools/MySek`
    return this.http.get(url, { observe: 'response' }).toPromise();
  }
   
  
}
