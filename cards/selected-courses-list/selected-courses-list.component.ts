import { Component, ViewEncapsulation, Input, Output, OnInit } from '@angular/core';
// import { Student } from '../../../../models';
import { startWith } from 'rxjs/operators';
import { SelectedCourseList, Course } from '../../models';
import { RefactorCDVService, NotificationService } from '../../services';
import * as _ from "underscore";


@Component({
    selector: 'card-selected-courses',
    templateUrl: './selected-courses-list.component.html',
    styleUrls: ['./selected-courses-list.component.scss']
	// encapsulation: ViewEncapsulation.None
})
export class SelectedCoursesListCardComponent implements OnInit {
    @Input() course: Course;
    @Input() id_student: string;
    
    is_paid: boolean = false;

    constructor(
        private refactorCDVService: RefactorCDVService,
        public notifier: NotificationService    
    ) {}

    ngOnInit(): void {
        this.is_paid = (_.find(this.course.announcements, c => c.paid === true)) ? true : false;
    }


    async deleteCourse(id_opportunity: string, event) {

        event.stopPropagation();

        let delete_course = await this.refactorCDVService.deleteCourse(id_opportunity);

        if(delete_course.ok) {
            const student = await this.refactorCDVService.getStudent(this.id_student);
            this.notifier.raise('onLoadStudentAllData', student.body);
        }

    }

    // public sendDialogInfo( dialogrRoute: string ) {
    //     this.onButtonOpenDialog_click(dialogrRoute, this.person);
    // }

}