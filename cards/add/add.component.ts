import { Component, ViewEncapsulation, Input, Output, OnInit } from '@angular/core';
import { NotificationService } from '../../services';
// import { Student } from '../../../../models';

@Component({
    selector: 'card-add',
    templateUrl: './add.component.html',
    styleUrls: ['../../summer-course.component.scss']
	// encapsulation: ViewEncapsulation.None
})
export class AddCardComponent implements OnInit {
    @Input() type: string;
    @Input() id_family?: string;

    constructor(
        private notifier: NotificationService,
    ) {}

    ngOnInit(): void {}

    openDialog(): void {


        let data = (this.type !== 'tutor') ? "new" : this.id_family

        this.notifier.raise('onButtonOpenDialog_click', {
            'route': (this.type === 'tutor') ? 'tutor_new' : 'student_info',
            'data': data
        });
        

    }
}