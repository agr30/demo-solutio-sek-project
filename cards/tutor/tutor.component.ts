import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { Tutor, Family } from '../../models';
import { CardComponentWithActions } from '../cardwithactions.component';
import { NotificationService } from '../../services';


@Component({
    selector: 'card-tutor',
    templateUrl: './tutor.component.html',
    styleUrls: ['../../summer-course.component.scss']
	// encapsulation: ViewEncapsulation.None
})

export class TutorCardComponent extends CardComponentWithActions implements OnInit {

    @Input() tutor: Tutor;
    @Input() family: Family;
    @Input() admin_view: Boolean = false;
    // public is_tutor = false;

    constructor(public notifier: NotificationService) {
        super(notifier);
    }

    ngOnInit(): void {
        // this.is_tutor = this.person instanceof Tutor;
    }

    public sendDialogInfo( dialogrRoute: string, data: Tutor | Family | any, family: Family ) {
        data = {data, family};
        this.onButtonOpenDialog_click(dialogrRoute, data);
    }

    goToTerms(): void {
        if(!this.tutor.logged) return;
        this.notifier.raise('goToTerms', this.tutor);
    }



}
