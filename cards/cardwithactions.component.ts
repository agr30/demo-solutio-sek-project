import { Output, EventEmitter } from '@angular/core';
import { NotificationService } from '../services';
import { Tutor, Student, Course } from '../models';
// import { Grade } from '../../../models/grade.model';



export class CardComponentWithActions {

    constructor(public notifier: NotificationService) {}


    public onButtonOpenDialog_click(route: string, data?: Tutor | Student | Course | any ): void {

        const dialogInfo = {
            route,
            data
        };
        
        this.notifier.raise('onButtonOpenDialog_click', dialogInfo);

    }
}
