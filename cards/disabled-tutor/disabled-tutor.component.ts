import { Component, ViewEncapsulation, Input, Output } from '@angular/core';
import { Tutor } from '../../models';
import { StorageService } from '../../../../../../../core/storage/storage.service';
import { ConfigResourceService } from '../../../../../../../core/config/config.resource.local';
import { AdmissionService } from '../../../../admission.service';

@Component({
    selector: 'card-disabled-tutor',
    templateUrl: './disabled-tutor.component.html',
	styleUrls: ['../../summer-course.component.scss']
})
export class DisabledTutorCardComponent{
    @Input() tutor: Tutor;
    // @Input() family_name: string;
    @Input() id_family: string;
    @Input() logged_tutor_full_name: string;

    urlApp: string;

    constructor(
        private storageService: StorageService,
        public config: ConfigResourceService,
        private admissionService: AdmissionService
    ) {

    }

    ngOnInit(): void {
        this.urlApp = this.config.getUrlApp();
    }


    requestPermission() {

        const request = {
            tutorName: this.tutor.full_name,
            familyName: this.logged_tutor_full_name,
            url: `${this.urlApp}/request-permission?ChangeId=${this.tutor.id}&familyId=${this.id_family}&isSummerCourse=true`,
            email: this.tutor.contact.email
        };

        const data = JSON.stringify(request);

        this.admissionService.requestPermission(data).subscribe();
    }
}