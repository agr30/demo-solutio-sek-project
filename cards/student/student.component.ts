import { Component, ViewEncapsulation, Input, Output, OnInit, SimpleChanges, OnChanges, DoCheck } from '@angular/core';
import { Student, Course, Family } from '../../models';
import { NotificationService } from '../../services';
import { CardComponentWithActions } from '../cardwithactions.component';
import { Router } from '@angular/router';
import * as _ from "underscore";
import { ConfigResourceService } from '../../../../../../../core/config/config.resource.local';



@Component({
    selector: 'card-student',
    templateUrl: './student.component.html',
    styleUrls: ['../../summer-course.component.scss']
	// encapsulation: ViewEncapsulation.None
})
export class StudentCardComponent extends CardComponentWithActions implements OnInit, OnChanges {
    @Input() student: Student;
    @Input() family: Family;
    @Input() admin_view: Boolean = false;

    public txtCourse: string;
    hiddenAlerts: any[]  = [];
    canPay: boolean = false;
    cdvActive: boolean = true;

    constructor(notifier: NotificationService,
        public config: ConfigResourceService,
                private router: Router) {
        super(notifier);
        this.cdvActive = config.getCdvActive();
    }

    closeAlert(type: string) {
        this.hiddenAlerts.push(type);
    }


    ngOnInit(): void {

    }

    ngOnChanges(){
        let courses_to_paid = _.find(this.student.courses, e => e.id_receipt === null) ;
        this.canPay = (this.student.courses.length === 0) ? false : (!courses_to_paid) ? false : true;
    }

	showOptionsCdv(){
		return this.admin_view ? true : (this.cdvActive) ? true : false;

	}
    public sendDialogInfo( dialogrRoute: string, data: Student | Course | Family | any, id_student?, id_course? ) {
        let admin_view = this.admin_view;
        if (dialogrRoute === 'course_details') {

            let id_receipt = _.find(this.student.courses, e => e.id === id_course).id_receipt;

            if (id_receipt) {
                this.notifier.raise('openReceipt', id_receipt);

            } else {
                if (id_student) {
                    data = { data, id_student, admin_view};
                    if (id_course) data.id_course = id_course;
                }
                this.onButtonOpenDialog_click(dialogrRoute, data);
            }
        }else {

            if (id_student ){
                data = { data, id_student, admin_view };
                if (id_course) data.id_course = id_course;
            }
            this.onButtonOpenDialog_click(dialogrRoute, data);

        }
    }
}
