import { Component, OnInit, OnDestroy } from '@angular/core';
// import { SummerCourseService } from './summer-course.service';
import { Family, Tutor, Student, Course } from './models';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Dialog } from './dialogs/dialog.component';
import { NotificationService, RefactorCDVService } from './services';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageService } from '../../../../../core/storage/storage.service';
import * as _ from 'underscore';
import { Location } from '@angular/common'



@Component({
  selector: 'app-summer-course',
  templateUrl: './summer-course.component.html',
  styleUrls: ['./summer-course.component.scss'],
  // providers: [ReservationPaymentService]

})
export class SummerCourseComponent implements OnInit, OnDestroy {
  
  public family: Partial<Family>;
  public dialogComponent: string;

  logged_tutor_full_name: string;

  admin_view: boolean = false;

  sharing_tutors: Partial<Tutor>[] = [];
  privates_tutors: Partial<Tutor>[] = [];
  
  constructor(
    private refactorCDVService: RefactorCDVService,
    private notifier: NotificationService,
    public router: Router,
    public storageService: StorageService,
    public activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private location: Location) {

      
      
      // this.notifier.on<Dialog>('onDialogDestroy').subscribe(d => {
        // console.log('Dialog cerrado', d);
      // });

      this.notifier.on<any>('openReceipt').subscribe( r => { 
        this.dialog.closeAll();
        this.dialogComponent = 'courses_receipt';
        this.openDialog(r);
      })

      this.notifier.on<Student>('onStudentSaved').subscribe(student => { 
        this.family.students.push(new Student( student ));
      });
      
      this.notifier.on<Tutor>('onTutorSaved').subscribe(tutor => { 
        this.family.tutors.push(new Tutor( tutor ));

        this.sharing_tutors = this.filterTutorSharingInfo(true);
        this.privates_tutors = this.filterTutorSharingInfo(false);

      });
      
      this.notifier.on<Tutor>('onTutorUpdated').subscribe(params => { 
        var tutor: Tutor;
        if(params[0]){
          tutor = params[0];
          this.family = new Family(JSON.parse(JSON.stringify( params[1])));
        }  else{
          tutor = params;
        }
        Object.assign(_.find(this.family.tutors, e => e.id === tutor.id), new Tutor(tutor));
        this.family.marital_status_fam = tutor.marital_status
        this.family = new Family(JSON.parse(JSON.stringify(this.family)));
        this.sharing_tutors = this.filterTutorSharingInfo(true);
        this.privates_tutors = this.filterTutorSharingInfo(false);
        console.log('this.family', this.family);
      });

      this.notifier.on<Student>('onLoadStudentAllData').subscribe(params => { 
        var student: Student;
        if(params[0]){
          student = params[0];
          this.family = new Family(JSON.parse(JSON.stringify( params[1])));
        }  else{
          student = params;
        }
        Object.assign(_.find(this.family.students, e => e.id === student.id), new Student(student));
        // this.family = Object.assign({}, this.family);
        this.family = new Family(JSON.parse(JSON.stringify(this.family)));
        this.setSelected();
        console.log('this.family', this.family);
      });
    
      this.notifier.on<any>('onButtonOpenDialog_click').subscribe(dialogInfo => { 
        this.dialog.closeAll();
        this.dialogComponent = dialogInfo.route;
        this.openDialog(dialogInfo.data);
        
      });

    
      this.notifier.on<any>('goToTerms').subscribe((tutor:Tutor) => {
        if(this.admin_view == false){
          this.router.navigateByUrl(`/terms-conditions/${this.storageService.retrieveSession('individualId')}/${this.storageService.retrieveSession('idFamilySelected')}/summer_course`);
        }
    
      });
    
  }
  
  async ngOnInit() { 
    if (this.activatedRoute.snapshot.params.paymentid) {
      const paymentid = this.activatedRoute.snapshot.params.paymentid
      
      if (this.activatedRoute.snapshot.params.paymentid.substr(0, 9) === "paymentko") {
        const paymentidnumber = paymentid.substr(9,paymentid.length)
        // await this.refactorCDVService.updateStatusPayment(paymentidnumber, false);
      } else {
       
        let payment = await this.refactorCDVService.getPayment(paymentid);
        this.notifier.raise('openReceipt', paymentid);
        // if(payment.status === 200){
        //   if (payment.body.status === 'InProgress') {
        //     // await this.refactorCDVService.updateStatusPayment(paymentid, true);
        //     // this.notifier.raise('openReceipt', paymentid);
        //   } 
        // } else {
        //   // this.notifier.raise('openReceipt', paymentid);
        // }
        
      }

		  this.location.replaceState(this.router.createUrlTree(['/summer-module/summer-courses']).toString());

    }

    if (this.activatedRoute.snapshot.params.id_family) { 
      this.admin_view = true;
      this.family = new Family(await this.refactorCDVService.getFamily(this.activatedRoute.snapshot.params.id_family));
      this.storageService.storeSession('idFamilySelected', this.family.id);
    } else {
      this.family = new Family(await this.refactorCDVService.getFamily(this.storageService.retrieveSession('idFamilySelected')));
    }

    
    (this.family.tutors || []).forEach(t => {
      if (t.contact.email == this.storageService.retrieveSession('email')){
        this.logged_tutor_full_name = t.full_name;
        this.storageService.storeSession('userDocumentNumber', t.document_number); 
        this.storageService.storeSession('userDocumentType', t.document_type); 
        this.storageService.storeSession('userPhoneNumber', t.contact.phone); 

      } 
      return t.logged = t.contact.email == this.storageService.retrieveSession('email'); 
    });
 

    this.setSelected();
  
    console.log( 'this.family', this.family );

    this.sharing_tutors = this.filterTutorSharingInfo(true);
    this.privates_tutors = this.filterTutorSharingInfo(false);

  }

  
  
  public setSelected() {

    _.chain(this.family.students).pluck("courses")
      .flatten().pluck("announcements")
      .flatten().each(e => e.selected = true);

    _.chain(this.family.students).pluck("courses")
      .flatten().pluck("additional_ser")
      .flatten().each(e => e.selected = true);

  }

  // async updateStatusPayment(paymentid){
  //   let payment = await this.refactorCDVService.getPayment(paymentid);
  //   if(payment.status === 200){
  //     if (payment.body.status === 'InProgress') await this.refactorCDVService.updateStatusPayment(paymentid, true);
  //   }
  // }

  filterTutorSharingInfo(type: boolean): Partial<Tutor>[] {
    
    if (type) {   
      return this.family.tutors.filter(x => x.sharing_info == type || x.logged);
    } else {
      return this.family.tutors.filter(x => x.sharing_info == type && !x.logged);
    }

  }


  openDialog(data?: Tutor | Student | Course | Partial<Family> | string): void {

    const dialogRef = this.dialog.open(Dialog, {
      panelClass: 'panel-right',
      data: {
        component: this.dialogComponent,
        data: data
      }
    });
    
  }

  ngOnDestroy() {

    if(this.admin_view) {
      this.storageService.clearSession('idFamilySelected');
    }

  }
  
}