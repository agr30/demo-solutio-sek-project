import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdmissionService } from '../../admission.service';
import { AppSharedModule } from '../../../../../core/shared/shared.module';
import { SummerCourseService } from './summer-course.service';
import { SummerCourseRoutingModule } from './summer-course-routing.module';
import { SummerCourseComponent } from './summer-course.component';
import { StudentInformationService } from '../../modules/studentInformation/studentInformation.service';
import { SummerCourseGuard } from './summer-course-guard.guard';
import { SharedStudentTutorModule } from '../shared-student-tutor/shared-student-tutor.module'
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { TutorCardComponent } from './cards/tutor/tutor.component';
import { AddCardComponent } from './cards/add/add.component';
import { DisabledTutorCardComponent } from './cards/disabled-tutor/disabled-tutor.component';
import { StudentCardComponent } from './cards/student/student.component';
import { SelectedCoursesListCardComponent } from './cards/selected-courses-list/selected-courses-list.component';
import { AnnouncementComponent } from './components/announcement/announcement.component';
import { PurchaseListComponent } from './components/purchase-list/purchase-list.component';
import { AdminActionsComponent } from './components/admin-actions/admin-actions.component';
import { TutorInformationComponent, Dialog, StudentInformationComponent, TutorNewComponent } from './dialogs'
import { CoursesListComponent, CourseDetailsComponent, CoursesFinderComponent } from './dialogs'
import { CoursesPaymentComponent, CoursesReceiptComponent } from './dialogs'
import { NotificationService, RefactorCDVService  } from './services';
import { CdvBackOfficeComponent } from './back-office/cdv-back-office.component';


// import { AuthGuardTutorInformation } from '../../modules/tutorInformation/auth-guard-tutorInformation.service';
// import { AuthGuardStudentInformation } from '../../modules/studentInformation/auth-guard-studentInformation.service';
// import { AuthGuardSharedDocumentation } from '../../modules/sharedDocumentation/auth-guard-sharedDocumentation.service';
// import { AuthGuardAddDocumentation } from '../../modules/addDocumentation/auth-guard-addDocumentation.service';
// import { AuthGuardAdmissionInProcess } from '../../modules/admissionInProcess/auth-guard-admissionInProcess.service';
// import { TermsConditionsSCComponent } from '../../components/terms-conditions-sc/terms-conditions-sc.component';
// import { AddDocumentationModule } from '../../modules/addDocumentation/addDocumentation.module';
// import { ReservationPaymentModule } from '../../modules/reservationPayment/reservationPayment.module';
// import { ChangeAdmissionModule } from '../../modules/changeAdmissionStatus/changeAdmissionStatus.module';
// import { ModalRequestAccessDocsComponent } from '../../components/modal-request-access-docs/modal-request-access-docs.component';
import { CdvBackOfficeRoutingModule } from './back-office/cdv-back-office-routing.module';


@NgModule({
  declarations: [
    SummerCourseComponent,
    TutorCardComponent,
    AddCardComponent,
    DisabledTutorCardComponent,
    StudentCardComponent,
    SelectedCoursesListCardComponent,
    Dialog,
    TutorInformationComponent,
    StudentInformationComponent,
    CoursesListComponent, 
    AnnouncementComponent,
    PurchaseListComponent,
    AdminActionsComponent,
    CourseDetailsComponent,
    CoursesFinderComponent,
    CoursesPaymentComponent,
    CoursesReceiptComponent,
    TutorNewComponent,
    CdvBackOfficeComponent
    // TermsConditionsSCComponent  ,
    // ModalRequestAccessDocsComponent
  ],
  imports: [
    AppSharedModule,
    SummerCourseRoutingModule,
    CommonModule,
    PDFExportModule,
    SharedStudentTutorModule,
    CdvBackOfficeRoutingModule
  ],
  providers: [
    SummerCourseGuard,
    AdmissionService,
    SummerCourseService,
    StudentInformationService,
    AdmissionService,
    Dialog,
    NotificationService,
    RefactorCDVService
  ],
  exports: [
    SharedStudentTutorModule
  ],
  entryComponents: [
    Dialog,
    TutorInformationComponent,
    StudentInformationComponent,
    CoursesListComponent,
    AnnouncementComponent,
    PurchaseListComponent,
    AdminActionsComponent,
    CourseDetailsComponent,
    CoursesFinderComponent,
    CoursesPaymentComponent,
    CoursesReceiptComponent,
    TutorNewComponent
  ]
})
export class SummerCourseModule { }
