import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ConfigResourceService } from '../../../../../core/config/config.resource.local';
import { Exception } from '../../../../model/exception';
import { FormGroup } from "@angular/forms";
import { SimpleMQ } from "ng2-simple-mq";
import { Subject } from 'rxjs/Subject';
import { resolve } from 'url';
import { Family } from './models';



@Injectable()
export class SummerCourseService {
  token: string;
  servicesUrl: string;
  graphServicesUrl: string;
  currentUser;
  summerCoursesUrl: string; 
  // data: any = require('/assets/mocks/family.json')

  constructor(
    private http: HttpClient,
    public config: ConfigResourceService,
    public smq: SimpleMQ) {
    this.summerCoursesUrl = config.getUrlSummerCourses();
    this.servicesUrl = config.getServicesUrl();
    this.graphServicesUrl = config.getGraphServicesUrl(); 
  }

  // Service management error
  public handleError(error: HttpResponse<any> | any): Observable<any> {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof HttpResponse) {
      const err = error['error'] || JSON.stringify(error);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    const errorService: Exception = new Exception('handle', errMsg);
    return Observable.throw(errorService);
  }

  //// servcios de user recovery 2
  validarDNIServidor(document: string) {
    return this.http.get(`${this.graphServicesUrl}/api/users/checkdocument/${document}/NIF`)
      .map( (response: any) => {
            return response;
      });
  }

  public enviardDataUserRecovery(document, email): Observable<any> {

    return this.http
      .get(`${this.graphServicesUrl}/api/users/bydocument/${document}/NIF/${email}`)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }
  //// fin servicios user recovery

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////                 SUMMER COURSES              /////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ///////////////////////////////////////////////  COLLEGES  ///////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getAllCollegesAndThematic(): Observable<any> {

    return this.http
      .get(this.summerCoursesUrl + '/api/Schools/GetSchoolThematic')
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }

  public checkAdmission(): Observable<any> {

    return this.http
      .get(this.summerCoursesUrl + '/api/CheckAdmission')
        .map((response: any) => {
          const result: any = {};
          const body = response;
          return response;
        })
        .catch(this.handleError);

  }
  
  async checkAdmissionAsync(): Promise<any> {

    const response = await this.http.get(this.summerCoursesUrl + '/api/CheckAdmission').toPromise();
    return response;

  }


  ////////////////////////////////////////////get////////////////////////////////////////////////////////////////////// 
  ////////////////////////////////////////////  SEND INVITATION  ///////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public createNewUserSendInvitation(data: any): Observable<any> {

    return this.http
      .post(this.graphServicesUrl + '/api/users/admission/register', data)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ///////////////////////////////////////////////  FAMILIES  ////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getFamilySummerCourse(): Observable<any> {

    return this.http
      .get(this.summerCoursesUrl + '/api/Families')
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  // async getFamilySummerCourseAsync(): Promise<any> {
  //   const response = await this.http.get(this.summerCoursesUrl + '/api/Families').toPromise();
  //   debugger;
  //   return response.json().bpi[currency].rate;
  // }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public createFamilySummerCourse(data): Observable<any> {

    return this.http
      .post(this.summerCoursesUrl + '/api/Families/Insert', data)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ///////////////////////////////////////////////  TUTORS  ////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getSummerTutors(idFamilySummerCourse) {

    return this.http
      .get(this.summerCoursesUrl + '/api/Tutors/GetAllByFamily/' + idFamilySummerCourse)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public createSummerTutor(familyId, data): Observable<any> {

    return this.http
      .post(this.summerCoursesUrl + '/api/Tutors/Insert/' + familyId, data)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getTutorInfo(tutorId): Observable<any> {

    return this.http
      .get(this.summerCoursesUrl + '/api/Tutors/' + tutorId) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getTutorInfoNoToken(tutorId): Observable<any> {

    return this.http
      .get(this.summerCoursesUrl + '/api/Tutors/GetNoToken/' + tutorId) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public updateTutorInfo(tutorId, data): Observable<any> {

    return this.http
      .post(this.summerCoursesUrl + '/api/Tutors/Update?tutorId=' + tutorId, data) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public updateTutorNoToken(tutorId, data): Observable<any> {

    return this.http
      .post(this.summerCoursesUrl + '/api/Tutors/UpdateNoToken?tutorId=' + tutorId, data) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public insertNewTutorInfo(familyId, data): Observable<any> {

    return this.http
      .post(this.summerCoursesUrl + '/api/Tutors/Insert/' + familyId, data) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getIdsFromEmail(mail){
    let url = `${this.config.getUrlIDS()}/users?filter=${mail}`;
    return this.http.get(url)
        .map((res: any) => {
            return res;
        });
}
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public updateTutor(id: string, data: any): any {
      let url = `${this.config.getUrlIDS()}/users/${id}`;
      return this.http.patch(url, data)
          .map((res: any) => {
              return res;
          });
  }


  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ///////////////////////////////////////////////  STUDENTS  ////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getSummerStudents(idFamilySummerCourse) {

    return this.http
      .get(this.summerCoursesUrl + '/api/Students/ByFamily/' + idFamilySummerCourse)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public insertNewStudentInfo(familyId, data): Observable<any> {

    return this.http
      .post(this.summerCoursesUrl + '/api/Students/Insert/' + familyId, data) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public updateStudentInfo(studentId, data): Observable<any> {

    return this.http
      .post(this.summerCoursesUrl + '/api/Students/Update/?studentId=' + studentId, data) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getSummerStudent(idStudent) {

    return this.http
      .get(this.summerCoursesUrl + '/api/Students/' + idStudent)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

 

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ///////////////////////////////////////////////   COURSES ////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getSummerCourses(ageMax, ageMin, schools, thematic) {
    //https://mysekdev.azure-api.net/cursos-verano-dev-svc/api/SummerCourses/Filter?AgeMax=1&AgeMin=2&Schools=3&Thematic=4
    return this.http
      .get(this.summerCoursesUrl + '/api/SummerCourses/Filter?AgeMax=' + ageMax + '&AgeMin=' + ageMin + '&Schools=' + schools + '&Thematic=' + thematic)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public filterSummerCourses(ageMax, ageMin, schools, thematic) {

    return this.http
      .get(this.summerCoursesUrl + '/api/SummerCourses/Filter?AgeMax=' + ageMax + '&AgeMin=' + ageMin + '&Schools=' + schools + '&Thematic=' + thematic)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }

  public deleteCourse(idCourseAsocciated) {

    return this.http
      .post(`${this.summerCoursesUrl}/api/SummerCourses/Delete/${idCourseAsocciated}`, null )
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }


  public saveConcept(data): Observable<any> {

    return this.http
      .post(`${this.summerCoursesUrl}/api/Salesforce/Concepto`, data)
      .map((response: any) => {
        return response;
      })

      .catch(this.handleError);
  }

  public savePaymentReceipt(idREceiptSalesForce, data): Observable<any> {

    return this.http
      .post(`${this.summerCoursesUrl}/api/Payments/Insert/${idREceiptSalesForce}`, data)
      .map((response: any) => {

        return response;
      })

      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getConvocatoriesByCourseId(courseID) {

    return this.http
      .get(this.summerCoursesUrl + '/api/SummerCourses/GetTerms/' + courseID)
      .map((response: any) => {
        //const result: any = {};
        //const body = response;
        return response;
      })
      .catch(this.handleError);

  }

  public sendPairsToServer(pairs: any[]) {
    return this.http
      .post(`${this.summerCoursesUrl}/api/Salesforce/Export`, pairs)
      .map(response => response)
      .catch(this.handleError);
  }

  public exportToSF(studentId, courseId) {

    return this.http
      .get(`${this.summerCoursesUrl}/api/Salesforce/Export/${studentId}/${courseId}`)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public loadCoursePriceAlexia(data) {

    //console.log("DATOS ENVIADOS");
    //console.log(data);

    return this.http
      .post(this.summerCoursesUrl + '/api/Alexia/GetCotizacionAlumnoSEK', data) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }

  public getCollectives() {

    return this.http
      .get(this.summerCoursesUrl + '/api/Collectives/GetAllFromSF')
      .map((response: any) => {
        //const result: any = {};
        //const body = response;
        console.log("Resp serv collectivos: ");
        console.log(response);
        if(response === null)
          return response;
        else
          return response;
      })
      .catch(this.handleError);

  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public addCourseToStudent(studentId, summerCourseId, data) {

    return this.http
      .post(this.summerCoursesUrl + '/api/SummerCourses/Insert/' + studentId + '/' + summerCourseId, data) // TODO id Tutor dinamico
      .map((response: any) => {
        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getSummerCourseByStudentId(summerCourseId) {

    return this.http
      .get(`${this.summerCoursesUrl}/api/SummerCourses/${summerCourseId}`)
      .map((response: any) => {
        return response;
      })
      .catch(this.handleError);

  }

  public getSummerCoursesByStudentsId(ids: number[]): Promise<any[]> {
    let count = ids.length - 1;
    let result = [];
    let promise = new Promise<any[]>(resolve => {
      ids.forEach(i => this.getSummerCourseByStudentId(i).subscribe(r => {
        result.push(r);
        if (!count--) resolve(result);
      }));
    });
    return promise;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public updateStudentSummerCourse(idCourseAsocciated, data) {
    return this.http
      .post(this.summerCoursesUrl + '/api/SummerCourses/Update?id=' + idCourseAsocciated, data) // TODO id Tutor dinamico
      .map((response: any) => {
        console.log('idCourseAsocciated serv', idCourseAsocciated);
        console.log('data serv', data);

        const result: any = {};
        const body = response;
        return response;
      })
      .catch(this.handleError);

  }
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public requestPermission(data): Observable<any> {

    return this.http
      .post(this.graphServicesUrl + '/api/users/admission/requestPermissions', data)
      .map((response: any) => {
        const result: any = {};
        const body = response;
        this.smq.publish('error-mq', 'Se ha enviado un correo al destinatario informándole de la solicitud de permiso para acceder a su información.');
        return response;
      })
      .catch(this.handleError);

  }




  // HARD CODE SERVICES //

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /*
  public getSummerCourses():any{

    const response = {
      "courses" : [
        {
          "id" : "1",
          "typeCourse" : "National",
          "name" : "Little Artist",
          "age" : "De 4 años a 8 años" ,
          "duration" : "Mes o quincena",
          "image" : "example_image.png"
        },
        {
          "id" : "2",
          "typeCourse" : "International",
          "name" : "Sports Camp",
          "age" : "De 6 años a 16 años" ,
          "duration" : "Mes o quincena",
          "image" : "example_image2.png"
        }
      ]
    }

    return response;  
  }
  */

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public getLocationCourses(): any {

    const response = {
      "courses": [
        {
          "national": [
            { "name": "Ciudalcampo Madrid Norte" },
            { "name": "Catalunya (Barcelona)" },
            { "name": "El Castillo" }
          ],
        },
        {
          "international": [
            { "name": "Ciudalcampo2 Madrid Norte" },
            { "name": "Catalunya2 (Barcelona)" },
            { "name": "El Castillo2" }
          ]
        }
      ]
    }

    return response;

  }

}
