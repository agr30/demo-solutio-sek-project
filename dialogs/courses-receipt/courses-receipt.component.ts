import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'underscore';
import { ConfigResourceService } from '../../../../../../../core/config/config.resource.local';
import { schollsConstants } from '../../../../../../common/consts/schollsConstants';
import { Tutor } from '../../models';
import { RefactorCDVService } from '../../services';




@Component({
	selector: 'courses-receipt-dialog',
	templateUrl: './courses-receipt.component.html',
	styleUrls: ['./courses-receipt.component.scss', '../../components/purchase-list/purchase-list.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class CoursesReceiptComponent implements OnInit {

	paymentid: string;
	logged_tutor: Partial<Tutor>;
	receipt: any;
	total_amount = 0;
	scale = 0.8;
	currentEnvironment: string;
	schoolReceiptData: any;

	constructor(
		public dialogRef: MatDialogRef<CoursesReceiptComponent>,
		public translate: TranslateService,
		private refactorCDVService: RefactorCDVService,
		public config: ConfigResourceService,
	) {
		this.currentEnvironment = config.getCurrentEnvironment();
		this.paymentid = this.dialogRef.componentInstance['data'].data;
	}

	ngOnInit(): void {

		this.getPayment();
		
	}

	public exportReceiptToPdf(exporter: any): void {
		
		exporter.saveAs(`receipt.pdf`);
		
	}

	async changeStatusPaid(){
		await this.refactorCDVService.updateStatusPayment(this.paymentid, true);
	}

	async getPayment(){
		let payment = await this.refactorCDVService.getPayment(this.paymentid);

		if(payment.status === 200){
			this.receipt = payment.body;
			this.setSchoolInfo(payment.body.id_mysek)
			if (this.receipt.status === 'InProgress') this.changeStatusPaid();
		}
	}

	setSchoolInfo(school) {
		this.schoolReceiptData  = (this.currentEnvironment === 'uat') ? _.chain(schollsConstants).find(e => e.idUAT === school).value() : _.chain(schollsConstants).find(e => e.idPRO === school).value();

	}

	closeDialog() {
		this.dialogRef.close();
	}
}
