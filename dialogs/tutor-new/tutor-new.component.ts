import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { MatDialogRef, ErrorStateMatcher } from '@angular/material';
import { DateAdapter } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors, FormGroupDirective, NgForm, AbstractControl } from '@angular/forms';
// import { NotificationService, StudentService } from '../../../../services';
import { Student } from '../../models';
import { StorageService } from '../../../../../../../core/storage/storage.service'
import { TranslateService } from '@ngx-translate/core';
import { RefactorCDVService, NotificationService } from '../../services';
import { AdmissionService } from '../../../../admission.service';
import { validarDNI, validarNIE } from '../../../../../../common/validations';
import { translateTemplates } from '@swimlane/ngx-datatable/release/utils';


export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
		return !!(control && control.invalid);
	}
}

export interface ConstantsValues {
	active: boolean | null;
	label: string;
	value: string;
}

@Component({
	selector: 'tutor-new-dialog',
	templateUrl: './tutor-new.component.html',
	styleUrls: ['tutor-new.component.scss'],
})
export class TutorNewComponent implements OnInit, OnDestroy {

	forma: FormGroup;
	id_family: string;
	matcher = new MyErrorStateMatcher();
	document_types: ConstantsValues[];
	user_exist: boolean = false;
	message_error: string = "";
	private storageService: StorageService;
    
	constructor(
		// private dateAdapter: DateAdapter<Date>,
		public dialogRef: MatDialogRef<TutorNewComponent>,
		public translate: TranslateService,
    	private notifier: NotificationService,
		private refactorCDVService: RefactorCDVService,
		private admissionService: AdmissionService,
		public injector: Injector,
		// private fb: FormBuilder,
		// private service: StudentService
    ) {
		this.storageService = this.injector.get(StorageService);
		this.id_family = this.dialogRef.componentInstance['data'].data

		this.forma = new FormGroup({
			'first_name': new FormControl('', [Validators.required]),
			'first_surname': new FormControl('', [Validators.required]),
			'document_type': new FormControl('', [Validators.required]),
			'document_number': new FormControl('', [Validators.required]),
			'contact': new FormGroup({
				'email': new FormControl('', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")])
			})
		});

		this.forma.valueChanges.subscribe(e => this.user_exist = false);

		this.forma.controls['document_type'].valueChanges
			.subscribe((document_type) => {

				this.forma.get('document_number').clearValidators();
				this.forma.get('document_number').updateValueAndValidity({ onlySelf: false, emitEvent: false });

				if (document_type === 'NIF') {
					this.forma.get('document_number').setValidators(validarDNI);
				} else if (document_type === 'NIE') {
					console.log('entra en NIE');
					this.forma.get('document_number').setValidators(validarNIE);
				} else if (document_type === 'Pasaporte' || document_type === 'Otros') {
					this.forma.get('document_number').setValidators([Validators.required]);
				} 


				this.forma.get('document_number').updateValueAndValidity({ onlySelf: false, emitEvent: false });
			});
	}
    
    
	ngOnInit(): void {
		this.getStaticData();
	}
	
	getStaticData(): void {

		const currentLang = this.storageService.getCookie('Lang') || 'es';

		this.admissionService.getStaticData("Contact", ['Document_Type__c', 'Marital_Status__c'], currentLang)
			.subscribe(result => {
				this.document_types = result.find(s => { return s.name === "Document_Type__c" }).picklistValues;
			});

	}
	
	async create(){

		try {
			const save =  await this.refactorCDVService.createTutor(this.id_family, this.forma.value);
			
			if (save.ok) {
				const new_tutor = await this.refactorCDVService.getTutor(save.body);
				this.notifier.raise('onTutorSaved', new_tutor.body)
			}

			this.closeDialog();
		
		} 
		
		catch (error) {

			if(error.status === 409){
				this.message_error = error.error.errorMessage;
				this.user_exist = true;
			}

	  	}

	}

	getErrorMessage(control: FormControl | AbstractControl): string {
		let message = ""
		if (control) {
			if (control.hasError('validarDNI')){
				this.translate.get('message.error.invalidDNI').subscribe(res => {
					message = res;
				});
				return message;
			} 
			if (control.hasError('validarNIE')){
				this.translate.get('message.error.invalidDocument').subscribe(res => {
					message = res;
				});
				return message;
			} 
			if (control.hasError('required')){
				this.translate.get('message.error.enterValue').subscribe(res => {
					message = res;
				});
				return message;
			} 
		}
	}

	closeDialog() {
		this.dialogRef.close();
	}

	ngOnDestroy(): void {}

}
