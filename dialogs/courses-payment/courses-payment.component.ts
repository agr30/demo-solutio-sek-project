import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, ErrorStateMatcher, MatExpansionPanel } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors, FormGroupDirective, NgForm } from '@angular/forms';

import * as _ from 'underscore';

import { Course, Thematic, Discount, Announcements, Checkout, Tutor } from '../../models';
import { Student, Family } from '../../models';
import { schollsConstants } from '../../../../../../common/consts/schollsConstants';
import { ConfigResourceService } from '../../../../../../../core/config/config.resource.local';
import { ReservationPaymentService } from "../../../reservationPayment/reservationPayment.service";
import { validateIBAN } from "../../../../../../common/validations";
import { RefactorCDVService } from '../../services';
import { StudentCheckout } from '../../models/checkout.model';
import { NotificationService } from '../../services';
import { BankAliasService } from '../../../../../../components/bank-alias/bank-alias.service';
import { BankAliasComponent } from '../../../../../../components/bank-alias/bank-alias.component';
import { StorageService } from '../../../../../../../core/storage/storage.service';


@Component({
	selector: 'courses-payment-dialog',
	templateUrl: './courses-payment.component.html',
	styleUrls: ['courses-payment.component.scss'],
	providers: [ReservationPaymentService]
})
export class CoursesPaymentComponent implements OnInit, AfterViewInit {

	@ViewChild(BankAliasComponent) bankAlias: BankAliasComponent;

	@ViewChild('panel_tpv') panel_tpv: MatExpansionPanel;
	@ViewChild('panel_directDebit') panel_directDebit: MatExpansionPanel;
	@ViewChild('panel_international') panel_international: MatExpansionPanel;

	family: Family;
	tutor: Partial<Tutor>;
	statusBankForm: boolean;
	saved: boolean = false;

	paymentType: string;
	currentEnvironment: string;
	MERCHANT_COD: any;
	orderId: string;
	reservationPaymentService: ReservationPaymentService;
	checkout: StudentCheckout[] = [];
	amount: any;
	schoolsToPay: Set<string>;
	international: boolean;
	internationalReservation: boolean;
	frmIbanAccount: FormControl;
	frmTitularCuenta: FormControl;
	guid: string;
	short_guid: string;
	school: string;
	bankAccounts: any[];
	countryType: string;
	internationalTransfer: string;
	backofficeFlag : boolean = false;
	
	directDebitNationalCourse: boolean;
	directDebitInternationalCourse: boolean;
	fieldsValid: any;

	pestana: string;
	param = {value: ''};
	familyName: string;
	servicesRefactorSM: string;
	accountsShow: boolean = false;
	schools: string[];

	constructor(
		public dialogRef: MatDialogRef<CoursesPaymentComponent>,
		public config: ConfigResourceService,
		public injector: Injector,
		private refactorCDVService: RefactorCDVService,
		public bankAliasService : BankAliasService,
		public notifier: NotificationService,
		public storageService: StorageService
	) {
		this.reservationPaymentService = this.injector.get(ReservationPaymentService);
		this.family = this.dialogRef.componentInstance['data'].data;
		this.currentEnvironment = config.getCurrentEnvironment();
		this.servicesRefactorSM = this.config.getServicesRefactorSM_URL();
	}


	ngOnInit(){  
		this.familyName = this.family.name;
		this.countryType = 'ALL';
		this.checkRequiredFields();
		this.tutor = _.find(this.family.tutors, function(e){
			return e.logged == true;
		});
		if (this.storageService.retrieveSession('adminRole')) this.backofficeFlag = true;
		if (!this.tutor){
			this.tutor = this.family.tutors[0];
			this.tutor.logged = true;
		} 
		this.param = {value: this.internationalTransfer };  
		
	}

	ngAfterViewInit(){
		
		this.getAccounts();
	}

	onChangeSchoolsToPay(event){
		this.schoolsToPay = event;
		if(	this.schools == undefined){
			this.getSchools(); 
		}
		else{
			this.setInternationalAccount();

		}
	
	}



	onChangeStatusBankForm(event){ 
    
		this.directDebitNationalCourse = this.directDebitInternationalCourse = false;
		this.statusBankForm = event;

		// kco aqui inicio
		if (this.bankAlias.getSelectedAccountCountry() == "Ninguno de la lista") {
			 
			// this.paymentType = "internationalTransfer";
			this.countryType = 'ALL';
			this.directDebitInternationalCourse = true;

			this.bankAlias.hideInputPay();
			
			 
			// setTimeout(() => {
			// 	// this.panel_directDebit.close();
			// 	//this.panel_international.open();
			// }, 150);

		} else {
		//kco aqui fin
		//this.bankAccounts = alias;
			this.bankAlias.showInputPay();
			if(!this.accountsShow){
				this.getAccounts();
			
				if(this.noBankAliasAndSelectedSepaAccount()){
					if(this.international){
						this.directDebitInternationalCourse = true;
					} else {
						this.directDebitNationalCourse = true;
					}
				} 
			} 
		}  
	}

	async getSchools(){ 
	 
		let schools = await this.refactorCDVService.getSchools(); 
		if(schools.status === 200){
			this.schools = schools.body.records;
			this.setInternationalAccount();
		}
	}

	async pay(){
		this.getSchoolToPay();
		if(this.paymentType === 'directDebit'){
			this.saved = true;
		}

		const item = {
			final_price: this.amount,
			method: (this.paymentType === ('bankCard')) ? "TPV" : (this.paymentType === ('internationalTransfer')) ? "Wire Transfer" : "Automatic Debit Payment",
			students: this.checkout,
			id_mysek: this.school,
			iban: (this.paymentType === ('directDebit')) ? this.bankAlias.getSelectedIban() : undefined,
			holder: (this.paymentType === ('directDebit')) ? this.bankAlias.getSelectedAccountHolder() : undefined
		}

		const body = new Checkout(item);
		const result = await this.refactorCDVService.makePayment(body);

		if (this.paymentType !== 'internationalTransfer') {

			this.guid = result.body.toString();
			this.short_guid = this.guid.substr(0,12);
			this.afterPayment();
		}
		if (this.paymentType == 'internationalTransfer') {
			this.closeDialog();
		}
	}

	async afterPayment(){

		if(this.paymentType === 'bankCard') { 
			let schoolInfo =
				this.currentEnvironment === "uat"
				? _.chain(schollsConstants)
					.find(e => e.idUAT === this.school.toString())
					.value()
				: _.chain(schollsConstants)
					.find(e => e.idPRO === this.school.toString())
					.value();

			this.MERCHANT_COD = schoolInfo.tpv.merchantCod;

			this.send_form();
		}
		else{
			this.update();
			this.notifier.raise('openReceipt', this.guid);
		}
	}

	async update(){
		for ( let i = 0; i < this.checkout.length; i++){
			const student = await this.refactorCDVService.getStudent(this.checkout[i].id);
			this.notifier.raise('onLoadStudentAllData', student.body);
		}
		
	}

	getSchoolToPay(){
		if(this.schoolsToPay.size > 1){
			this.school = (this.currentEnvironment === "uat") ? "2213" : "679";
		}else{
			this.school = this.schoolsToPay.values().next().value;
		}
	}

	public getAccounts(): any{

		this.bankAliasService.getTutorBankAccounts(this.family.id)
			.subscribe(alias => {
				this.bankAccounts = alias;
				this.countryType = "ALL";
				this.checkSepaCountry();
				this.accountsShow = true
			});
	}



	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private checkSepaCountry(){

		let sepaCount = 0;
		this.countryType = "NO_SEPA"; 
		if(this.bankAccounts && this.bankAccounts.length > 0){
			for(let i = 0; i < this.bankAccounts.length; i++){
				if(this.bankAccounts[i].bank_Account_Country__c != "Ninguno de la lista"){
					sepaCount++;
				}
			}
			if(sepaCount == 0){
				this.countryType = "NO_SEPA";
			} 
			else if(sepaCount == this.bankAccounts.length){
				this.countryType = "SEPA";
			} 
			else {
				this.countryType = "ALL"; // Hay alias con paises SEPA, NO_SEPA o sin pais.
			}
		}
		else {
			this.countryType = "NO_COUNTRY";
		}

	}

	public setInternationalAccount(){ 
		if(this.schoolsToPay.size > 0){
			this.getSchoolToPay();
			let schoolData =
				this.currentEnvironment === "uat"
				? _.chain(this.schools)
					.find(e => e.id_mysek === this.school.toString())
					.value()
				: _.chain(this.schools)
					.find(e => e.id_mysek === this.school.toString())
					.value(); 
			
			this.internationalTransfer = schoolData.name + ": " + schoolData.internationalTransfer;
			this.param = {value: this.internationalTransfer };  
		}
		
	}

	inputValidate(paramsInput) {

		if (!paramsInput.merchantCode) throw new Error("The merchant code is mandatory");
		if (!paramsInput.terminal) paramsInput.terminal = 1;
		if (!paramsInput.currency) paramsInput.currency = "978";
		let payerConcept = this.tutor.first_name + ' ' + this.tutor.first_surname + ' ' + this.tutor.second_surname;

		let url = (this.currentEnvironment === 'uat') ? 'https://weu-wbap-mysek-uat-01.azurewebsites.net' : 'https://mysekadmisiones.sek.es';
	
		//url = 'http://localhost:4200';
	
		const paramsObj = {
		  DS_MERCHANT_AMOUNT: String(paramsInput.amount),
		  DS_MERCHANT_ORDER: paramsInput.order,
		  DS_MERCHANT_MERCHANTCODE: paramsInput.merchantCode,
		  DS_MERCHANT_CURRENCY: "978",
		  DS_MERCHANT_TRANSACTIONTYPE: "0",
		  DS_MERCHANT_TERMINAL: "1",
		  DS_MERCHANT_TITULAR: payerConcept,
		  DS_MERCHANTURL: `${url}/admissions-module`,
		  DS_MERCHANT_URLOK: `${url}/summer-module/summer-courses/cdv${this.guid}`,
		  DS_MERCHANT_URLKO: `${url}/summer-module/summer-courses/cdvpaymentko${this.guid}`,
		  DS_MERCHANT_MERCHANTURL: `${this.servicesRefactorSM}/Payment/Bank?from=cdv`
		 
		};
		 
	
		// https://weu-wbap-mysek-uat-01.azurewebsites.net
		// http://localhost:4200
	
		return paramsObj;
	
	}

	send_form = function () {

		const obj = {
		  amount: parseFloat(this.amount).toFixed(2).replace(/[.,\s]/g, ''),
		  order: this.short_guid,
		  merchantCode: this.MERCHANT_COD
		};
	
		const paramsObj = this.inputValidate(obj);
		const payload = JSON.stringify(paramsObj);
	
	
		this.reservationPaymentService.getTPVParams(payload)
		  .subscribe(result => {
			let frm = document.createElement("form");
			frm.method = 'POST';
			document.body.appendChild(frm);
	
			// Ds_SignatureVersion
			let Ds_SignatureVersionInput = document.createElement("input");
			Ds_SignatureVersionInput.type = "hidden";
			Ds_SignatureVersionInput.name = "Ds_SignatureVersion";
			Ds_SignatureVersionInput.value = "HMAC_SHA256_V1";
			frm.appendChild(Ds_SignatureVersionInput);
	
			// Ds_MerchantParameters
			let Ds_MerchantParametersInput = document.createElement("input");
			Ds_MerchantParametersInput.type = "hidden";
			Ds_MerchantParametersInput.name = "Ds_MerchantParameters";
			Ds_MerchantParametersInput.value = result.params;
			frm.appendChild(Ds_MerchantParametersInput);
	
			// DS_MERCHANTPARAMETERS
			let Ds_SignatureInput = document.createElement("input");
			Ds_SignatureInput.type = "hidden";
			Ds_SignatureInput.name = "Ds_Signature";
			Ds_SignatureInput.value = result.signature;
			frm.appendChild(Ds_SignatureInput);
	
			frm.action = (this.currentEnvironment === 'uat') ? "https://sis-t.redsys.es:25443/sis/realizarPago" : "https://sis.redsys.es/sis/realizarPago";
			// frm.action = "https://sis-t.redsys.es:25443/sis/realizarPago"; // test
			// frm.action = "https://sis.redsys.es/sis/realizarPago"; // real
			frm.submit();
		  });
	};

	
	private noBankAliasAndSelectedSepaAccount(){
		return (this.paymentType && this.paymentType === "directDebit" && this.statusBankForm && this.bankAccounts && this.bankAccounts.length === 0 && this.bankAlias.getSelectedAccountCountry() == "Ninguno de la lista");
	}

	async checkRequiredFields(){

		const response = await this.refactorCDVService.checkRequiredFields(this.family.id)
		this.fieldsValid = response.body;

	}
	

	validStatusPayment(): boolean{
		
		const invalidPayment = !this.checkout.length || !this.paymentType || 
		(this.paymentType && this.paymentType === "directDebit" && !this.statusBankForm) || 
		this.noBankAliasAndSelectedSepaAccount();
		// ||
		// (this.paymentType && this.paymentType === "internationalTransfer");

		const invalidTerms = !this.getValidStatusTutors();
		if(this.backofficeFlag) return invalidPayment;
		return invalidPayment || invalidTerms
	}

	getValidStatusTutors(){
		return (this.getTermsTutors().length === 1 && this.getFlagMaritalStatus()) || (this.getTermsTutors().filter( a => a ) && this.family.tutors.length === 1)
		|| this.getTermsTutors().length === 2
	}

	getTermsTutors(): boolean[]{
		return _.pluck(this.family.tutors, 'accepted_terms').filter( a => a)
	}

	getFlagMaritalStatus(): boolean{
		const maritalStatus = this.family.marital_status_fam;
		return (maritalStatus !== 'Divorced' && maritalStatus !== 'Ninguno' && maritalStatus !== 'Single') && maritalStatus !== null
	}


	closeDialog() {
		this.dialogRef.close();
	}
}
    