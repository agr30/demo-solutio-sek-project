import { Component, Inject, ViewChild, ComponentFactoryResolver, ViewContainerRef, ComponentRef } from '@angular/core';
import { Type, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogsSelectors } from '../consts/dialog-selectors.enum';
import { NotificationService } from '../services';
// import { DialogsRoutes } from '../../consts/dialog-routes.enum';


@Component({
    template: `<ng-template #target></ng-template>`,
})
export class Dialog implements OnInit, OnDestroy {

    @ViewChild('target', { read: ViewContainerRef }) vcRef: ViewContainerRef;

    componentRef: ComponentRef<any>;


    constructor(
        public dialogRef: MatDialogRef<Dialog>,
        private resolver: ComponentFactoryResolver,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private notifier: NotificationService
    ) {}

    ngOnInit() {
        
        this.loadDinamicComponent();
    }


    public loadDinamicComponent(): void {

        const factories = Array.from(this.resolver['_factories'].values());
        const factoryClass: any = factories.find(
            (x: any) => x.selector === DialogsSelectors[this.data.component]
        );
        
        const factory = this.resolver.resolveComponentFactory(factoryClass.componentType);
        this.componentRef = this.vcRef.createComponent(factory);

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnDestroy() {
        // this.notifier.raise('onDialogDestroy', this);
        if (this.componentRef) {
            this.componentRef.destroy();
        }
    }

}
