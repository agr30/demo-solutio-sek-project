import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'underscore';
import { Course, Family } from '../../models';
import { NotificationService } from '../../services';


@Component({
	selector: 'courses-list-dialog',
	templateUrl: './courses-list.component.html',
	styleUrls: ['./courses-list.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class CoursesListComponent implements OnInit {


	@Input() courses: Course[] = [];
	@Input() family: Partial<Family>;
	@Input() id_student: string;
	@Input() admin_view: Boolean = false;

	constructor(
		public translate: TranslateService,
		public notifier: NotificationService
	) {
	}

	differentCourses(): Partial<Course>[] {
		return _.chain(this.courses).uniq('id').value();
	}

	public onButtonOpenDialog_click(route: string, data?: Course): void { 
		let admin_view = this.admin_view;
		const dialogInfo = {
			route,
			data: {
				data: this.family,
				id_student: this.id_student,
				id_course: data.id,
				admin_view: admin_view
			}
		};

		this.notifier.raise('onButtonOpenDialog_click', dialogInfo);

	}

	ngOnInit(): void {}

}
