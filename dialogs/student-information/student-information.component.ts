import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef, ErrorStateMatcher } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors, FormGroupDirective, NgForm, Validator, AbstractControl } from '@angular/forms';
// import { NotificationService, StudentService } from '../../../../services';
import { Student, Family } from '../../models';
import { TranslateService } from '@ngx-translate/core';
import { RefactorCDVService, NotificationService } from '../../services';
import { AdmissionService } from '../../../../admission.service';
import { validarDNI, validarNIE } from '../../../../../../common/validations';
import { StorageService } from '../../../../../../../core/storage/storage.service';
import { MatNativeDateModule } from '@angular/material';
import {DateAdapter} from "@angular/material/core";


export interface ConstantsValues {
	active: boolean | null;
	label: string;
	value: string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
		// const isSubmitted = form && form.submitted;
		// return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
		return !!(control && control.invalid);
	}
}

@Component({
	selector: 'student-information-dialog',
	templateUrl: './student-information.component.html',
	styleUrls: ['student-information.component.scss'],
})
export class StudentInformationComponent implements OnInit, OnDestroy {

	forma: FormGroup;
	// subscription: Subscription;
	matcher = new MyErrorStateMatcher();
	student: Student;
	new_student: boolean = false;
	isAdmin: boolean;

	// TODO hacer tipo para los picklist
	staticData: any;
	document_types: ConstantsValues[];
	nationalities: ConstantsValues[];
	// provinces: ConstantsValues[];

	checking: boolean = false;

	id_family: string;

	// componente de alias de direcciones
	statusAddressForm: boolean = false;
	saved: boolean = false;
	id_new_student: string;

	constructor(
		// private dateAdapter: DateAdapter<Date>,
		public dialogRef: MatDialogRef<StudentInformationComponent>,
		public translate: TranslateService,
    private notifier: NotificationService,
		private refactorCDVService: RefactorCDVService,
		private admissionService: AdmissionService,
		private storageService: StorageService
		// private fb: FormBuilder,
		// private service: StudentService
	) {
		this.id_family = this.storageService.retrieveSession('idFamilySelected');
		this.isAdmin = this.storageService.retrieveSession('adminRole');


		if (this.dialogRef.componentInstance['data'].data === 'new') {
			this.new_student = true;
		} else {
			this.student = this.dialogRef.componentInstance['data'].data;
		}


		this.forma = new FormGroup({
			'first_name': new FormControl('', [Validators.required]),
			'first_surname': new FormControl('', [Validators.required]),
			'second_surname': new FormControl(''),
			'medical_information': new FormControl('', [Validators.required, this.ControlEmpty]),
			'birth_date': new FormControl('', [Validators.required]),
			'gender': new FormControl('', [Validators.required]),
			'document_type': new FormControl(''),
			'document_number': new FormControl(''),
			'nationality': new FormControl('', [Validators.required]) // ,
			// 'address': new FormGroup({
			// 	'residence': new FormControl(''),
			// 	'town': new FormControl(''),
			// 	'zip': new FormControl(''),
			// 	'province': new FormControl(''),
			// 	'country': new FormControl('')
			// })
		});

		// this.forma.get('address').valueChanges.subscribe( e => this.checkAddressValidations());

		this.forma.controls['document_type'].valueChanges
			.subscribe((document_type) => {

				this.forma.get('document_number').clearValidators();
				this.forma.get('document_number').updateValueAndValidity({ onlySelf: false, emitEvent: false });

				if (document_type === 'NIF') {
					this.forma.get('document_number').setValidators(validarDNI);
					this.forma.get('document_type').setValidators([Validators.required]);
				} else if (document_type === 'NIE') {
					this.forma.get('document_number').setValidators(validarNIE);
					this.forma.get('document_type').setValidators([Validators.required]);
				} else if (document_type === 'Pasaporte' || document_type === 'Otros') {
					this.forma.get('document_number').setValidators([Validators.required]);
					this.forma.get('document_type').setValidators([Validators.required]);
				} else {
					this.forma.get('document_type').clearValidators()
					this.forma.get('document_number').clearValidators()
				}

				this.forma.get('document_type').updateValueAndValidity({ onlySelf: false, emitEvent: false });
				this.forma.get('document_number').updateValueAndValidity({ onlySelf: false, emitEvent: false });
			});

		if( !this.new_student ) {
			this.forma.patchValue(this.student);
		}

	}

	ngOnInit(): void {
		// this.getProvinces();
		this.getStaticData();
		this.isDisabled();
		debugger
		// this.checkAddressValidations();
	}


	public ControlEmpty(control: FormControl): ValidationErrors {
		if (control.value != null) {
			if (control.value.trim() === '') {
			  return {
				ControlEmpty: {
				  valid: false
				}
			  };
			}
		}
		return null;
	  }

	getErrorMessage(control: FormControl | AbstractControl): string {
		if (control) {
			if (control.hasError('validarDNI')) return 'El documento introducido no es válido';
			if (control.hasError('validarNIE')) return 'El documento introducido no es válido';
			if (control.hasError('required')) return 'Debes introducir un valor';
		}
	}

	getStaticData(): void {

		let lang = this.storageService.getCookie('Lang');
		this.admissionService.getStaticData("Contact", ['X2nd_Language__c', 'Selected_Campus__c', 'hed__Citizenship__c', 'Document_Type__c',
			'Area_of_Interest__c', 'Pref__c', 'hed__Country_of_Origin__c', 'Professional_Position__c', 'Marital_Status__c', 'Occupation__c', 'Professional_Position__c '], lang)
			.subscribe(result => {
				// this.staticData = result;
				this.document_types = result.find(s => {return s.name === "Document_Type__c"}).picklistValues;
				this.nationalities = result.find(s => {return s.name === "hed__Citizenship__c"}).picklistValues;
			});

	}

	public isDisabled(): void {
	  if(!this.isAdmin) {
      this.student.courses.forEach(c => {
        c.announcements.forEach(a => {
          if (a.status === "Reserva de Plaza Pagada" || a.status === "Matriculado") this.forma.disable();
        })
      })
    }
  }

	async save() {

		const save = await this.refactorCDVService.saveStudent(this.forma.value);

		if (save.ok){
			this.saved = true;
			this.id_new_student = save.body;

			const new_student = await this.refactorCDVService.getStudent(save.body);
			this.notifier.raise('onStudentSaved', new_student.body);
		}

		this.closeDialog();
	}

	async update() {
	  const update =  await this.refactorCDVService.updateStudent(this.student.id, this.forma.value);
		if (update.ok) {
			this.saved = true;

			const student = await this.refactorCDVService.getStudent(this.student.id);
			this.id_family = this.storageService.retrieveSession('idFamilySelected');

			const familia = new Family(await this.refactorCDVService.getFamily(this.id_family));
			this.notifier.raise('onLoadStudentAllData', [student.body, familia ]);

		}

		this.closeDialog();
	}

	closeDialog() {

		this.dialogRef.close();
	}

	// checkAddressValidations(): void {

	// 	if(this.checking) return;

	// 	this.checking = true;

	// 	let isAllEmpty = true;
	// 	for (const key in this.forma.get('address')['controls']) {

	// 		if(key !== "country"){
	// 			if (this.forma.get(`address.${key}`).value.length !== 0) {
	// 				isAllEmpty = false;
	// 				break;
	// 			}
	// 		}

	// 	}

	// 	if (isAllEmpty) {
	// 		this.clearValidators();
	// 	} else {
	// 		this.setValidators();
	// 	}

	// }

	// getProvinces(): void {
	// 	this.admissionService.getProvinces().subscribe(prov => {
	// 		this.provinces = _.uniq(prov, 'value');
	// 	});
	// }

	// clearValidators(): void {

	// 	for (const key in this.forma.get('address')['controls']) {
	// 		this.forma.get(`address.${key}`).clearValidators();
	// 		this.forma.get(`address.${key}`).updateValueAndValidity({ onlySelf: false, emitEvent: false });
	// 	}
	// 	this.forma.get('address.country').setValue('');
	// 	this.checking = false;
	// }


	setValidators(): void {

		for (const key in this.forma.get('address')['controls']) {
			this.forma.get(`address.${key}`).setValidators(Validators.required);
			this.forma.get(`address.${key}`).updateValueAndValidity({ onlySelf: false, emitEvent: false });

		}
		this.forma.get('address.country').setValue('')
		this.forma.get('address.country').setValue('España');
		this.checking = false;
	}


	ngOnDestroy(): void {

	}


}
