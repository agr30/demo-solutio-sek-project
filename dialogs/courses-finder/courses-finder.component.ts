import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef, ErrorStateMatcher } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors, FormGroupDirective, NgForm, FormArray } from '@angular/forms';

import * as _ from 'underscore';

import { Course, Thematic, School } from '../../models';
import { Family } from '../../models/family.model';
import { RefactorCDVService } from '../../services';
import { ConfigResourceService } from '../../../../../../../core/config/config.resource.local';
import { environment } from '../../../../../../../../environments/environment';


@Component({
	selector: 'courses-finder-dialog',
	templateUrl: './courses-finder.component.html',
	styleUrls: ['./courses-finder.component.scss'],
})
export class CoursesFinderComponent implements OnInit {

	forma: FormGroup;
	courses: Course[] = [];
	jsonMOCK: Course[] = [];

	schools_nationals: Partial<School>[] = [];
	schools_internationals: Partial<School>[] = [];
	thematics: Partial<Thematic>[] = [];

	currentEnvironment: string;

	public family: Partial<Family>;
	public id_student: string;
	public student_birthDate: string;
	public admin_view: boolean;


	constructor(
		private fb: FormBuilder,
		public dialogRef: MatDialogRef<CoursesFinderComponent>,
		public translate: TranslateService,
		public config: ConfigResourceService,
		private refactorCDVService: RefactorCDVService
	) {

		this.forma = new FormGroup({
			'schools': new FormArray([]),
			'thematics': new FormArray([])
		});

		console.log(this.dialogRef.componentInstance['data'].data);
		this.family = this.dialogRef.componentInstance['data'].data.data;
		this.id_student = this.dialogRef.componentInstance['data'].data.id_student;
		this.admin_view= this.dialogRef.componentInstance['data'].data.admin_view;
		this.currentEnvironment = this.config.getCurrentEnvironment();

	}

	async ngOnInit() {
		let response = await this.refactorCDVService.getCourses();
		let courses;
		if(this.admin_view == false){
			 courses = response.body.filter( c => c.full_course === false )
		}
		else{
			 courses = response.body;
		}
		this.jsonMOCK = (courses || []).map(c => new Course(c));
		this.schools_nationals = this.filterCourseByLocation(false);
		this.schools_internationals = this.filterCourseByLocation(true);
		this.thematics = this.differentThematics();
		this.student_birthDate = this.getStudentBirthDate();
		this.setCoursesAvailable(this.student_birthDate);
	}

	filterCourseByLocation(type: boolean): Partial<School>[] {

		let order: string[];

		order = environment.orderSchools;

		if (type) {

			return _.uniq(_.chain(this.jsonMOCK)
				.pluck("school")
				.flatten()
				.filter(x => x.international == type)
				.value(), 'id');

		} else {

			return _.uniq(_.chain(this.jsonMOCK)
				.pluck("school")
				.flatten()
				.filter(x => x.international == type)
				.value(), 'id').sort((a, b) => {
					return (order.indexOf(a.id) - order.indexOf(b.id));
				});
		}

	}

	differentThematics(): Partial<Thematic>[] {

		return _.uniq(_.chain(this.jsonMOCK)
			.pluck("thematic")
			.flatten()
			.value(), 'id');

	}

	getStudentBirthDate() {
	  const student = this.family.students.find(s => s.id === this.id_student  );
    return student ? student.birth_date : null;
  }

  isCourseAvailable(course: Course, student_birthDate: string) {
	  const student_bd = new Date(student_birthDate);
	  const course_inDate = new Date(course.initial_birthdate);
	  const course_enDate = new Date(course.final_birthdate);

	  return student_bd >= course_inDate && student_bd <= course_enDate;
  }


  setCoursesAvailable(student_birthDate: string) {
    let auxArray = this.jsonMOCK.filter(c => this.isCourseAvailable(c, student_birthDate));
    this.jsonMOCK = auxArray;
  }

	selectThematic(id: String, isChecked: Boolean) {

		const thematics = <FormArray>this.forma.controls.thematics as FormArray;

		if (isChecked) {
			thematics.push(new FormControl(id));
		} else {
			let index = thematics.controls.findIndex(x => x.value == id);
			thematics.removeAt(index);
		}

		this.filterCourse();

	}


	selectSchool(id: Number, isChecked: Boolean) {

		const schools = <FormArray>this.forma.controls.schools as FormArray;

		if (isChecked) {
			schools.push(new FormControl(id));
		} else {
			let index = schools.controls.findIndex(x => x.value == id);
			schools.removeAt(index);
		}

		this.filterCourse();

	}


	filterCourse() {

		let courses: Course[] = this.jsonMOCK;

		if (this.forma.get('schools').value.length > 0) {
			courses = _.filter(courses, (course) => {
				return this.forma.get('schools').value.indexOf(course.school.id) !== -1;
			})
		}
		if (this.forma.get('thematics').value.length > 0) {
			courses = _.filter(courses, (c) => {
				const course_thematics = c.thematic.map(t => t.id);
				const values = this.forma.get('thematics').value;
				return (course_thematics.some(e => _.contains(values, e)));
			})
		}
		this.courses = courses;

		console.log('this.courses', this.courses);
	}

	closeDialog() {
		this.dialogRef.close();
	}

}
