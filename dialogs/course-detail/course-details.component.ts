import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef,ErrorStateMatcher } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors, FormGroupDirective, NgForm, Validator, AbstractControl } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import * as _ from 'underscore';
import { AdditionalService, AnnouncementsDetail, Course, CourseDiscount, Student, Tutor, CoursesOptions, Family } from '../../models';
import { NotificationService, RefactorCDVService } from '../../services';
import { AdmissionService } from '../../../../admission.service';
import { environment } from '../../../../../../../../environments/environment';
import { _ParseAST } from '@angular/compiler';
import { ConfigResourceService } from '../../../../../../../core/config/config.resource.local';
import { StorageService } from '../../../../../../../core/storage/storage.service';
 
export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean { 
		return !!(control && control.invalid);
	}
}


@Component({
	selector: 'course-details-dialog',
	templateUrl: './course-details.component.html',
	styleUrls: ['course-details.component.scss'],
})
export class CourseDetailsComponent implements OnInit {
	forma: FormGroup;
	matcher = new MyErrorStateMatcher();
	cdvActive: boolean = true;
	id_course: string;
	student: Student;
	tutors: Tutor[];
	course_detail: Course;
	course_discount: CourseDiscount;
	announcements_selected: AnnouncementsDetail[];
	services_selected: AdditionalService[];
	course_discount_flag: boolean = false;
	service_discount_flag: boolean = false;
	initial_amount: number = 0;
	final_amount: number = 0;
	is_new_course: boolean = true;
	seaCampFlag: boolean = false;
	courseOptions: CoursesOptions;
	flight_option: any;
	flight_origin: any;
	language_level: any;
	abroad_program: any;
	offerRouteOptions: any;
	family: Family;
	admin_view: Boolean = false;
	route_view: Boolean = false;
	private originalPrices: Map<string, number> = new Map<string, number>();
	anotherBothTurnsConvos: number;
	anotherOneTurnConvos: number;
	anotherTransportConvos: number;
	get CATALUNYA_PRICE(): number{
		return 14.75 
	} 

	get freeActivityFlag(): boolean {
		return !!environment.allowReview.join('').match(new RegExp((this.course_detail || { id: '' }).id, 'gi'));
	};

 

	constructor(
		public dialogRef: MatDialogRef<CourseDetailsComponent>,
		public translate: TranslateService,
		private admissionService: AdmissionService,
		public config: ConfigResourceService,
		private refactorCDVService: RefactorCDVService,
		public storageService: StorageService,
		private notifier: NotificationService
	) {
		this.cdvActive = config.getCdvActive(); 
		this.family = this.dialogRef.componentInstance['data'].data.data
		this.student = new Student(JSON.parse(JSON.stringify(this.dialogRef.componentInstance['data'].data.data.students.find(s => {
			return s.id === this.dialogRef.componentInstance['data'].data.id_student;
		})
		)));

		this.id_course = this.dialogRef.componentInstance['data'].data.id_course; 
		this.tutors = this.dialogRef.componentInstance['data'].data.data.tutors; 
		this.admin_view = this.dialogRef.componentInstance['data'].data.admin_view;
		
		this.forma = new FormGroup({
			'route': new FormControl('', [Validators.required]), 
		});
		

	}

	async ngOnInit() {
		this.setOptions();
		this.getStaticData();
		this.course_detail = new Course(JSON.parse(JSON.stringify(await this.refactorCDVService.getCourse(this.id_course, this.courseOptions))));
		this.anotherConvosByStundentAndCSchool();
		this.isSeaCampFormentera();
		this.isNewCourse();
		if (this.course_detail.school.international) this.insertPassport();
		if (!this.is_new_course) {
			if (this.course_detail.announcements.length == 1) {
				this.course_detail.announcements[0].selected = true;
			}
			this.setSelectedItems();
			if (this.course_detail.school.international) this.insertInterFields();
		} else {
			this.course_detail.announcements.map(a => a.selected = true);
		}
		this.saveOriginalPrices();
		this.onChange();
		this.forma.patchValue(this.course_detail);	
	}

	anotherConvosByStundentAndCSchool() {
		let courses : Course[]
		if(this.student.courses.length){
			const idSChool = this.schoolOfCourse()
			const both = 2, one = 1
			courses = this.student.courses.filter(c => c.school.id === idSChool && c.id !== this.id_course);
			this.anotherTransportConvos = this.getAnotherConvosOfTransport(courses)
			this.anotherBothTurnsConvos = this.getAnotherConvosOfTurns(courses, both)
			this.anotherOneTurnConvos = this.getAnotherConvosOfTurns(courses, one)
		} 
		
	}

	getAnotherConvosOfTransport(courses){
		return courses.reduce((a, b) => {
			let sum
			 if(b.additional_ser.find(as => as.id === environment.transportService)){
				 sum = a + b.announcements.length
			 }else{
				 sum = a
			 } return sum }, 0);
	}

	getAnotherConvosOfTurns(courses, quantity){
		return courses.reduce((a, b) => {
			let sum
			 if(b.additional_ser.filter(as => as.id !== environment.transportService).length === quantity){
				 sum = a + b.announcements.length
			 }else{
				 sum = a
			 } return sum }, 0);
	}



	schoolOfCourse() {
		return this.course_detail.school.id
	}

	showOptionsCdv(){ 
		return this.admin_view ? true : (this.cdvActive) ? true : false; 
		 
	}

	saveOriginalPrices(){
		this.course_detail.additional_ser.forEach(cd => {
			this.originalPrices.set(cd.id, cd.price);
		})
	}

	areBothTurns() {
		const result = _.filter(this.course_detail.additional_ser, (s: any) => {
			return (s.id === "01t26000005ZrHaAAK" || s.id === "01t26000005ZrHbAAK" || s.id === "01t1n00000EAw1nAAD" || s.id === "01t1n00000EAw1iAAD") && s.selected
		})
		return result.length === 2
	}

	setPrices() {
		
		this.course_detail.additional_ser.forEach(cd => cd.price = this.originalPrices.get(cd.id));
		if (this.areBothTurns()) {
			this.course_detail.additional_ser.map(as => {
				if (as.id !== environment.transportService) as.price = this.isCatalunya() ? this.CATALUNYA_PRICE : this.roundFiveOrTenth(as.price * 0.84)
			})
			this.setServicePrices();
		} else {
			const turn = this.course_detail.additional_ser.find(as => as.id !== environment.transportService)
			this.course_detail.additional_ser.map(as => {
				if (as.id !== environment.transportService) as.price = turn.price
			})
			this.setServicePrices();
		}
	}

	public setServicePrices() {
		let anotherConvos = 0;
		this.course_detail.additional_ser.map(as => {
			if(as.id === environment.transportService){
				anotherConvos = this.anotherTransportConvos
			}else if(this.areBothTurns){
				anotherConvos = this.anotherBothTurnsConvos
			}else{
				anotherConvos = this.anotherOneTurnConvos
			}
			as.price = this.setPriceByTerms(as, anotherConvos) 
		})
	}

	public setPriceByTerms(as: Partial<AdditionalService>, anotherConvos: number) {
		const course = this.course_detail;
		const transport = as.id === environment.transportService
		const percentage = as.id !== environment.transportService ? 0.78 : 0.66
		const selected = course.announcements.filter(an => an.selected)
		const convos = (selected.length + anotherConvos > 4) ? 4 : selected.length + anotherConvos
		switch (convos) {
			case 2:
				return this.isAlboranOrCatalunya() && !transport? Math.round(as.price * 2 * percentage) / 2 : this.roundFiveOrTenth(as.price * 2 * percentage) / 2
			case 3:
				let price = this.isAlboranOrCatalunya() && !transport? Math.round(as.price * 2.68 * percentage) / 3 : this.roundFiveOrTenth(as.price * 2.68 * percentage) / 3
				if(this.areBothTurns() && !this.isAlboranOrCatalunya()) price += 0.6666666666666667
				return price
			case 4:
				return this.isAlboranOrCatalunya() && !transport? Math.round(as.price * 3.33 * percentage) / 4 : this.roundFiveOrTenth(as.price * 3.33 * percentage) / 4
			default:
				return as.price
		}
	}

	roundFiveOrTenth(amount) {
		const multiple10 = Math.round(amount / 10) * 10;
		const multiple5 = Math.round(amount / 5) * 5;
		const diff10 = Math.abs(amount - multiple10);
		const diff5 = Math.abs(amount - multiple5);
		diff10 < diff5 ? amount = multiple10 : amount = multiple5;
		return amount
	}

	isCatalunya(): boolean{
        const id = this.course_detail.school.id_mysek;
        return id === environment.catalunya
	}
	
	
    isAlboranOrCatalunya(): boolean{
        const id = this.course_detail.school.id_mysek;
		return id === environment.catalunya || id === environment.alboran
    }

	setOptions(){

		const options = {
			id_student : this.student.id,
			check_paid : true,
			use_alexia_prices: true
		}
		this.courseOptions = new CoursesOptions(options);
		
	}
		
	async getStaticData(){
		 
		let lang = this.storageService.getCookie('Lang');
		await this.admissionService.getStaticData("Opportunity", ['Flight_Option__c', 'Flight_Origin__c', 'Language_Level__c', 'Abroad_Program_Realized__c', 'Route_Type__c'], lang)
			.subscribe(result => {
				this.flight_option = result.find(s => { return s.name === "Flight_Option__c" }).picklistValues;
				this.flight_origin = result.find(s => { return s.name === "Flight_Origin__c" }).picklistValues;
				this.language_level = result.find(s => { return s.name === "Language_Level__c" }).picklistValues;
				this.abroad_program = result.find(s => { return s.name === "Abroad_Program_Realized__c" }).picklistValues;
				let offerRouteOpt = result.find(s => { return s.name === "Route_Type__c" }).picklistValues;
				this.offerRouteOptions = offerRouteOpt.filter(s => s.value !== "Not necessary");
			});
	}

	onChange(index?) {
		
		if (this.seaCampFlag && typeof index === 'number') this.setExcludingCheckbox(index);
		this.setPrices();
		this.getAnnouncementsSelected();
		this.getServicesSelected();
		this.getAmountBeforeDiscounts();
		this.applyCourseDiscount();
		this.showRouteType();
	}
	showRouteType(){
		this.route_view = false; 
		
		const route = this.services_selected.find(as => as.id == "01t26000006FzyuAAC");
		if(route){ 
			this.route_view = route.selected;  
		} 
		else{
			this.course_detail.route = null;
		}
	}

	setRouteType(event){
		this.course_detail.route = event.value;
	}
	setExcludingCheckbox(index) {
		this.course_detail.announcements.map(a => {
			a.selected = false;
		})
		this.course_detail.announcements[index].selected = true;
	}


	removeNotSelected() {

		this.course_detail.announcements = _.chain(this.course_detail.announcements)
			.filter(c => c['selected']).value();

		this.course_detail.additional_ser = _.chain(this.course_detail.additional_ser)
			.filter(c => c['selected']).value();
		if(this.course_detail.route == null ){
			this.course_detail.route = "Not necessary";
		}

	}

	async addCourse() {

		this.removeNotSelected();
		const save = await this.refactorCDVService.saveCourse(this.student.id, this.course_detail);
		if (save.ok) {
			const student = await this.refactorCDVService.getStudent(this.student.id);
			this.notifier.raise('onLoadStudentAllData', student.body);
		}

		this.closeDialog();
	}

	async editCourse() {

		this.removeNotSelected();
		const edit = await this.refactorCDVService.editCourse(this.student.id, this.course_detail);
		if (edit.ok) {
			const student = await this.refactorCDVService.getStudent(this.student.id);
			this.notifier.raise('onLoadStudentAllData', student.body);
		}

		this.closeDialog()
	}

	setSelectedItems() {

		let selected_announcements_id: string[] = _.find(this.student.courses, (e) => e.id === this.id_course).announcements.map(e => {
			return e.id
		})

		_.filter(this.course_detail.announcements, (convocatoria: AnnouncementsDetail) => selected_announcements_id.includes(convocatoria.id))
			.forEach(e => {
				e.selected = true;
			});

		// additional_serv
		let selected_additional_serv_id: string[] = _.find(this.student.courses, (e) => e.id === this.id_course).additional_ser.map(e => {
			return e.id
		})

		_.filter(this.course_detail.additional_ser, (additional_serv: AdditionalService) => selected_additional_serv_id.includes(additional_serv.id))
			.forEach(e => {
				e.selected = true;
			});

		let id_opportunity: string = _.find(this.student.courses, (e) => e.id === this.id_course).id_opportunity;
		this.course_detail.id_opportunity = id_opportunity;
	}

	insertInterFields() {
		this.course_detail.inter_fields = _.find(this.student.courses, (e) => e.id === this.id_course).inter_fields;
	}

	insertPassport() {
		if (this.student.document_type === "Pasaporte") this.course_detail.inter_fields.passport = this.student.document_number
	}

	isNewCourse() {
		let hasUSerCourse = _.some(this.student.courses, (course) => {
			return this.id_course === course.id;
		});
		this.is_new_course = !hasUSerCourse;
	}

	applyCourseDiscount(): any {

		this.course_discount = new CourseDiscount(this.course_detail, this.tutors, this.translate);
		if (this.course_discount) {
			this.setCourseDiscounts();
			this.setServicesDiscounts();
		}

	}

	getAnnouncementsSelected(): any {
		this.announcements_selected = _.filter(this.course_detail.announcements, (a: any) => {
			return a.selected;
		})
	}

	getServicesSelected(): any {
		this.services_selected = _.filter(this.course_detail.additional_ser, (a: any) => {
			return a.selected;
		})
	}

	getAmountBeforeDiscounts() {
		this.initial_amount = this.announcements_selected.reduce((s, f) => {
			return s + f.price;
		}, 0);
		this.initial_amount += this.services_selected.reduce((s, f) => {
			return s + f.price * this.announcements_selected.length;
		}, 0);
	}

	setCourseDiscounts() {
		if (this.course_discount.course_reduced.discounts.length) {
			this.course_discount_flag = true;
			this.final_amount = this.course_discount.course_reduced.final_amount
		} else {
			this.final_amount = this.initial_amount
		}
	}

	setServicesDiscounts() {
		if (this.course_discount.course_reduced.additional_services.length) {
			for (let i = 0; i < this.course_discount.course_reduced.additional_services.length; i++) {
				if (this.course_discount.course_reduced.additional_services[i].discounts.length) {
					this.service_discount_flag = true;
					this.final_amount += this.course_discount.course_reduced.additional_services[i].final_amount;
				} else {
					this.final_amount += this.course_discount.course_reduced.additional_services[i].initial_amount;
				}
			}
		}
	}

	isSeaCampFormentera() {
		const idCourseTemp = this.course_detail.id;
		if (idCourseTemp === environment.seaCampFormentera) this.seaCampFlag = true;
	}


	validInterFields() {
		if (this.course_detail.school.international) {
			this.resetInterFields()
			return this.course_detail.inter_fields.flight_option && this.validateFlightOption() && this.course_detail.inter_fields.language_level &&
					this.course_detail.inter_fields.abroad_program && this.validateAbroadProgram() && this.course_detail.inter_fields.passport
		} else {
			return true
		}

	}

	resetInterFields(){
		if (this.course_detail.inter_fields.flight_option !== 'Deseo que gestionen mi vuelo') this.course_detail.inter_fields.flight_origin = null
		if (this.course_detail.inter_fields.abroad_program !== 'Sí') this.course_detail.inter_fields.program_type_place_duration = null
	}

	validateFlightOption(): boolean{
		return ((this.course_detail.inter_fields.flight_option !== 'Deseo que gestionen mi vuelo' && this.course_detail.inter_fields.flight_origin === null)
				|| (this.course_detail.inter_fields.flight_option === 'Deseo que gestionen mi vuelo' && this.course_detail.inter_fields.flight_origin !== null))
	}

	validateAbroadProgram(): boolean{
		return ((this.course_detail.inter_fields.abroad_program !== 'Sí' && this.course_detail.inter_fields.program_type_place_duration === null)
				|| (this.course_detail.inter_fields.abroad_program === 'Sí' && this.course_detail.inter_fields.program_type_place_duration !== null))
	}

	async addAndPay(route: string) {

		this.removeNotSelected();
		const save = await this.refactorCDVService.saveCourse(this.student.id, this.course_detail);
		if (save.ok) {
			const student = await this.refactorCDVService.getStudent(this.student.id);
			Object.assign(_.find(this.family.students, e => e.id === student.body.id), new Student(student.body));
			this.family = new Family(JSON.parse(JSON.stringify(this.family)));
			this.setSelected();

			const dialogInfo = {
				route,
				data: this.family
			};
			this.notifier.raise('onLoadStudentAllData', student.body);
			this.notifier.raise('onButtonOpenDialog_click', dialogInfo);
		}
	}

	async editAndPay(route: string) {

		this.removeNotSelected();
		const edit = await this.refactorCDVService.editCourse(this.student.id, this.course_detail);
		if (edit.ok) {
			const student = await this.refactorCDVService.getStudent(this.student.id);
			Object.assign(_.find(this.family.students, e => e.id === student.body.id), new Student(student.body));
			this.family = new Family(JSON.parse(JSON.stringify(this.family)));
			this.setSelected();

			const dialogInfo = {
				route,
				data: this.family
			};
			this.notifier.raise('onLoadStudentAllData', student.body);
			this.notifier.raise('onButtonOpenDialog_click', dialogInfo);
		}
	}

	public setSelected() {

		_.chain(this.family.students).pluck("courses")
			.flatten().pluck("announcements")
			.flatten().each(e => e.selected = true);

		_.chain(this.family.students).pluck("courses")
			.flatten().pluck("additional_ser")
			.flatten().each(e => e.selected = true);

	}
	public  formValid(): boolean{
		if(this.route_view){
		   return this.forma.invalid;
		}
		return false;
	}

	closeDialog() {
		this.dialogRef.close();
	}
}
