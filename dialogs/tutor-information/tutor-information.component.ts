import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, ErrorStateMatcher, MatAutocompleteTrigger, MatAutocompleteSelectedEvent } from '@angular/material';
import { DateAdapter } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors, FormGroupDirective, NgForm, AbstractControl } from '@angular/forms';
// import { NotificationService, StudentService } from '../../../../services';
import { Subscription, Observable } from 'rxjs';
import { map, startWith, switchMap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Tutor, Collective } from '../../models';
import { TranslateService } from '@ngx-translate/core';
// import { validNIE, validNIF } from 'src/validators/validators';

import { SummerCourseService } from '../../summer-course.service';
import { RefactorCDVService, NotificationService } from '../../services';
import { AdmissionService } from '../../../../admission.service';
import { validarDNI, validarNIE, validarPHONE } from '../../../../../../common/validations';
import * as _ from "underscore";
import { StorageService } from '../../../../../../../core/storage/storage.service';
import { debug } from 'util';
import { Family } from '../../models/family.model';

export interface ConstantsValues {
	active: boolean | null;
	label: string;
	value: string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
		return !!(control && control.invalid);
	}
}

@Component({
	selector: 'tutor-information-dialog',
	templateUrl: './tutor-information.component.html',
	styleUrls: ['tutor-information.component.scss'],
})
export class TutorInformationComponent implements OnInit, OnDestroy {

	forma: FormGroup;
	matcher = new MyErrorStateMatcher();
	tutor: Tutor;
	new_tutor: boolean = false;
	collective: FormControl;
	collectives: Collective[];
	filteredCollectives: Collective[];
	subscription: Subscription;
	family: Family;

	@ViewChild(MatAutocompleteTrigger) trigger: MatAutocompleteTrigger;

	// TODO hacer tipo para los picklist
	staticData: any;
	document_types: ConstantsValues[];
	nationalities: ConstantsValues[];
	provinces: ConstantsValues[];
	marital_status: ConstantsValues[];

	checking: boolean = false;
	isAdmin: boolean;

	id_family: string;


	statusAddressForm: boolean = false;
	saved: boolean = false;
	idsCurrentTutor: string;


	constructor(
		// private dateAdapter: DateAdapter<Date>,
		public dialogRef: MatDialogRef<TutorInformationComponent>,
		public translate: TranslateService,
		// private fb: FormBuilder,
		private refactorCDVService: RefactorCDVService,
		private admissionService: AdmissionService,
		private summerCourseService: SummerCourseService,
		private notifier: NotificationService,
		public storageService: StorageService
	) {

		this.id_family = this.storageService.retrieveSession('idFamilySelected');
    this.isAdmin = this.storageService.retrieveSession('adminRole');

		if (this.dialogRef.componentInstance['data'].data.data === 'new') {
			this.new_tutor = true;
		} else {
			this.tutor = this.dialogRef.componentInstance['data'].data.data;
      this.family = this.dialogRef.componentInstance['data'].data.family;
		}

		// console.log('this.tutor', this.tutor);
		this.forma = new FormGroup({
			'alumni_SEK': new FormControl(''),
			'alumni_SEK_name': new FormControl(''),
			'sport_club': new FormControl(''),
			'collective': new FormGroup({
				'id': new FormControl(''),
				'name': new FormControl(''),
				'discount': new FormControl(''),
				'transport': new FormControl(''),
			}),
			'first_name': new FormControl('', [Validators.required]),
			'first_surname': new FormControl('', [Validators.required]),
			'second_surname': new FormControl(''),
			// 'birth_date': new FormControl(''),
			'marital_status': new FormControl('', [Validators.required]),
			'gender': new FormControl('', [Validators.required]),
			'document_type': new FormControl('', [Validators.required]),
			'document_number': new FormControl('', [Validators.required]),
			'nationality': new FormControl('', [Validators.required]),
			'contact': new FormGroup({
				'email': new FormControl(''),
				// , [Validators.required, Validators.pattern(/^[0-9]{9}$/)]
				'phone': new FormControl('', [Validators.required])
			}) // ,
			// 'address': new FormGroup({
			// 	'residence': new FormControl(''),
			// 	'town': new FormControl(''),
			// 	'zip': new FormControl(''),
			// 	'province': new FormControl(''),
			// 	'country': new FormControl('')
			// })
		});

		this.collective = <FormControl>this.forma.get('collective.name');

		this.forma.get('contact.phone').valueChanges.subscribe(phone => {

			if( !!phone ){
				this.forma.get('contact.phone').setValidators(validarPHONE);
			} else {
				this.forma.get('contact.phone').clearValidators();
				this.forma.get('contact.phone').setValidators([Validators.required]);
			}
			this.forma.get('contact.phone').updateValueAndValidity({ onlySelf: false, emitEvent: false });

		});

		// this.forma.get('address').valueChanges.subscribe(e => this.checkAddressValidations());

		this.forma.get('alumni_SEK').valueChanges.subscribe(alumni_SEK => {

			if (alumni_SEK) {
				this.forma.get('alumni_SEK_name').setValidators(Validators.required);

			} else {
				this.forma.get('alumni_SEK_name').clearValidators();
				this.forma.get('alumni_SEK_name').setValue('');
			}

			this.forma.get('alumni_SEK_name').updateValueAndValidity({ onlySelf: false, emitEvent: false });

		});

		this.forma.controls['document_type'].valueChanges
			.subscribe((document_type) => {
				if(document_type != null){
					this.forma.get('document_number').clearValidators();
					this.forma.get('document_number').updateValueAndValidity({ onlySelf: false, emitEvent: false });

					if (document_type === 'NIF') {
						this.forma.get('document_number').setValidators(validarDNI);
					} else if (document_type === 'NIE') {
						this.forma.get('document_number').setValidators(validarNIE);
					} else if (document_type === 'Pasaporte' || document_type === 'Otros') {
						this.forma.get('document_number').setValidators([Validators.required]);
					}

					this.forma.get('document_number').updateValueAndValidity({ onlySelf: false, emitEvent: false });
				}

			});

		if( !this.new_tutor ) {
			this.forma.patchValue(this.tutor);
		}

		this.collective.valueChanges.subscribe((value) => {
			this.filteredCollectives = (value.length < 2) ? [] : this.filter(value);
		});

	}

	ngOnInit(): void {

		// this.getProvinces();
		this.getCollectives();
		this.getStaticData();
		this.isDisabled();
		// this.checkAddressValidations();

	}

	async save() {

		const save = await this.refactorCDVService.saveTutor(this.forma.value);

		if (save.ok) {
			this.saved = true;

			const new_tutor = await this.refactorCDVService.getTutor(save.body);
			this.notifier.raise('onTutorSaved', new_tutor.body);
		}

		this.closeDialog();
	}

	async update() {
		const update =  await this.refactorCDVService.updateTutor(this.tutor.id, this.forma.value);

		if(update.ok){
			this.saved = true;
			const update_tutor = await this.refactorCDVService.getTutor(this.tutor.id);
			update_tutor.body.logged = this.tutor.logged;
			this.id_family = this.storageService.retrieveSession('idFamilySelected');

			const familia = new Family(await this.refactorCDVService.getFamily(this.id_family));
			this.notifier.raise('onTutorUpdated', [update_tutor.body, familia ]);
		}

		this.closeDialog();
	}


	filter(value: string): Collective[] {
		return _.filter(this.collectives, (coll: Collective) => coll.name.toLowerCase().includes(value.toLowerCase()));
	}

	public collectiveAssign(collective, event): void {

		if (event.isUserInput) {
			this.forma.get('collective').setValue( collective );
		}

	}

	getErrorMessage(control: FormControl | AbstractControl): string {
		if (control) {
			if (control.hasError('validarPHONE')) return 'El teléfono introducido no es válido';
			if (control.hasError('validarDNI')) return 'El documento introducido no es válido';
			if (control.hasError('validarNIE')) return 'El documento introducido no es válido';
			if (control.hasError('required')) return 'Debes introducir un valor';
		}
	}

  public isDisabled(): void {
    if(!this.isAdmin) {
      this.family.students.forEach(s => {
        s.courses.forEach(c => {
          c.announcements.forEach(a => {
            if (a.status === "Reserva de Plaza Pagada" || a.status === "Matriculado") this.forma.disable();
          })
        })
      })
    }
  }

	getStaticData( ):void {

		let lang = this.storageService.getCookie('Lang');

		this.admissionService.getStaticData("Contact", ['X2nd_Language__c', 'Selected_Campus__c', 'hed__Citizenship__c', 'Document_Type__c',
			'Area_of_Interest__c', 'Pref__c', 'hed__Country_of_Origin__c', 'Professional_Position__c', 'Marital_Status__c', 'Occupation__c', 'Professional_Position__c '], lang)
			.subscribe(result => {
				this.document_types = result.find(s => {return s.name === "Document_Type__c"}).picklistValues;
				this.nationalities = result.find(s => {return s.name === "hed__Citizenship__c"}).picklistValues;
			});

		this.admissionService.getStaticData("Account", ['Marital_Status_With_Other_Tutor__c'], lang)
		.subscribe(result => {
			this.marital_status = result.find(s => { return s.name === "Marital_Status_With_Other_Tutor__c" }).picklistValues;
		});

	}

	// getSpecificStaticData( name: string ): any {
	// 	return this.staticData.filter(e => e.name == name);
	// }

	getCollectives(): void {
		this.summerCourseService.getCollectives()
			.subscribe(result => this.collectives = result.records );
	}



	closeDialog() {
		this.dialogRef.close();
	}

	// metodos para forzar el click en la lista en el auitocompletar
	private _subscribeToClosingActions(): void {
		if (this.subscription && !this.subscription.closed) {
			this.subscription.unsubscribe();
		}

		this.subscription = this.trigger.panelClosingActions
			.subscribe(e => {
				if (!e || !e.source) {

					this.collective.setValue('');
					this.collectiveAssign({
						"id": null,
						"name": null,
						"discount": null,
						"transport": null
					}, true);
				}
			},
				err => this._subscribeToClosingActions(),
				() => this._subscribeToClosingActions());
	}

	handler(event: MatAutocompleteSelectedEvent): void {
		this.collective.setValue(event.option.value);
	}

	ngAfterViewInit() {
		this._subscribeToClosingActions();
	}

	ngOnDestroy() {
		if (this.subscription && !this.subscription.closed) {
			this.subscription.unsubscribe();
		}
	}

	// checkAddressValidations(): void {

	// 	if (this.checking) return;

	// 	this.checking = true;

	// 	let isAllEmpty = true;
	// 	for (const key in this.forma.get('address')['controls']) {

	// 		if (key !== "country") {
	// 			if (this.forma.get(`address.${key}`).value.length !== 0) {
	// 				isAllEmpty = false;
	// 				break;
	// 			}
	// 		}

	// 	}

	// 	if (isAllEmpty) {
	// 		this.clearValidators();
	// 	} else {
	// 		this.setValidators();
	// 	}

	// }

	// getProvinces(): void {
	// 	this.admissionService.getProvinces().subscribe(prov => {
	// 		this.provinces = _.uniq(prov, 'value');
	// 	});
	// }

	// clearValidators(): void {

	// 	for (const key in this.forma.get('address')['controls']) {
	// 		this.forma.get(`address.${key}`).clearValidators();
	// 		this.forma.get(`address.${key}`).updateValueAndValidity({ onlySelf: false, emitEvent: false });
	// 	}
	// 	this.forma.get('address.country').setValue('');
	// 	this.checking = false;
	// }


	// setValidators(): void {

	// 	for (const key in this.forma.get('address')['controls']) {
	// 		this.forma.get(`address.${key}`).setValidators(Validators.required);
	// 		this.forma.get(`address.${key}`).updateValueAndValidity({ onlySelf: false, emitEvent: false });
	// 	}
	// 	this.forma.get('address.country').setValue('')
	// 	this.forma.get('address.country').setValue('España');
	// 	this.checking = false;
	// }


}
