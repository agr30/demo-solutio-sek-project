import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { AdmissionService } from './admission.service';
import { StorageService } from '../../../../../core/storage/storage.service';
// import { AuthenticationService } from '../../../core/security/authentication.service';
import { ConfigLoader } from '@ngx-config/core';
import { SummerCourseService } from './summer-course.service';





@Injectable()
export class SummerCourseGuard implements CanActivate {

  constructor(
    // public admissionService: AdmissionService,
    private storageService: StorageService,
    public summerCourseService: SummerCourseService,
    // private authenticationService: AuthenticationService,
    public router: Router
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

    // admins no entran en cursos de verano ( ahora si )

    // if (this.storageService.retrieveSession('adminRole')){
    //   this.router.navigateByUrl('/admissions-module/admissions-in-process');
    //   return false;
    // }

    return true;


    }

  }

