import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Course, School } from '../models';
import { AdmissionService } from '../../../admission.service';
import { RefactorCDVService } from '../services/refactorCdV.service';
import { ConfigResourceService } from '../../../../../../core/config/config.resource.local';
import * as _ from "underscore";
import { Router } from '@angular/router';
import { StorageService } from '../../../../../../core/storage/storage.service';
import { environment } from '../../../../../../../environments/environment';
import { Subscription } from 'rxjs/Subscription';

 

@Component({
  selector: 'app-cdv-bo',
  templateUrl: './cdv-back-office.component.html',
  styleUrls: ['./cdv-back-office.component.scss'] 
})

export class CdvBackOfficeComponent implements OnInit {
  forma: FormGroup;
  all_courses: Course[] = [];
  all_courses_filters: Course[] = [];
  schools: Partial<School>[] = [];
  subscription: Subscription;
  currentEnvironment: string;
  noResults: Boolean = false;
  familiesData: any[];
  pageSize = 10; 
  lastMember = 0; 
  noMoreResults: Boolean = false; 
  //Scroll vars
  throttle = 300;
  scrollDistance = 0;
  // Scroll obj
  scrollObj: any; 
  scrollActive: Boolean = false; 
   
  
  constructor(
          public translate: TranslateService,
          private admissionService: AdmissionService,
          public config: ConfigResourceService,
          public storageService: StorageService,
          private refactorCDVService: RefactorCDVService,
          public router: Router
              ) {

    this.forma = new FormGroup({
      'text_filter': new FormControl(''),
      'admission_status': new FormControl(''),
      'courses': new FormControl(''),
      'schools': new FormControl('') 
    });
  
   
    this.forma.controls["schools"].valueChanges
    .subscribe((selectedSchool) => { 
      if(selectedSchool.length >0){
        this.all_courses_filters = this.all_courses.filter( s => _.contains(selectedSchool, s.school.id)); 
      }else{
        this.all_courses_filters = this.all_courses;  
      }
       
    });
   
  }

  async ngOnInit() {
    
    this.storageService.clearSession('idFamilySelected');

    this.currentEnvironment = this.config.getCurrentEnvironment();

    let all_courses = await this.admissionService.getCourses();
    this.all_courses_filters  =  this.all_courses = (all_courses.body || []).map(c => new Course(c));

    this.orderSchools();
    this.familiesData = []; 

  }

  public goToDashboard(id): void {
    console.log('entra');
    this.router.navigate(['/summer-module/summer-courses/admin-view/'+ id]);
  }
  
  public orderSchools(): void {

    let order: string[];

    order = environment.orderSchools
    
    let national =  _.uniq(_.chain(this.all_courses)
				.pluck("school")
				.flatten()
				.filter(x => x.international == false)
				.value(), 'id').sort((a, b) => {
					return (order.indexOf(a.id) - order.indexOf(b.id));
				});

    let international = _.uniq(_.chain(this.all_courses)
      .pluck("school")
      .flatten()
      .filter(x => x.international == true)
      .value(), 'id');

    this.schools = national.concat(international);   
  }

  public resultsFilter() {  
    this.noResults = false;
    this.lastMember = 0;
    this.noMoreResults = false;
    this.familiesData = [];
    this.callStudentBackService(); 
    // this.scrollActive =true; 
  }

  
  onScroll(even?) { 
    if (this.scrollActive){
      this.scrollObj = event;
      this.lastMember += 10;
  
      if (!this.noMoreResults ) {
        this.callStudentBackService();
      }
    }
  
  }  

  async  callStudentBackService( ) { 
    const save = await this.refactorCDVService.getBackoffice(this.forma.value, this.pageSize, this.lastMember)
     
		if (save.ok){
      console.log(save.body);
      this.noResults = false;
      if (save.body.items.length == 0) {
        this.noResults = true;
        this.noMoreResults = true;
      }  
      else{
        this.scrollActive = true;
        this.familiesData.push(...save.body.items); 
      }
		}else{
        this.noResults = true;
    }
  }
 
 
}
