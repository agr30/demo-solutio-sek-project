
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardAdmissionInProcess } from '../../userAdministration/auth-guard-userAdministration.service';
// import { AuthGuardAdmissionInProcess } from './auth-guard-userAdministration.service';
import { CdvBackOfficeComponent } from './cdv-back-office.component';

const routes: Routes = [
  {
    path: '',
    canLoad: [AuthGuardAdmissionInProcess],
    component: CdvBackOfficeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CdvBackOfficeRoutingModule { }